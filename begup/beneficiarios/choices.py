#Choice Fields
TIPO_BENEFICIARIOS = (
    (1, 'BEGUP - Estudiante'),
    (11, 'Persona con Discapacidad'),
    (21, 'Docente'),
)

TIPO_EVENTO_CREDITO = (
    ('P', 'Pedido'),#Se genera al crear el pedido
    ('A', 'Autorizado'),#lo realizan los operadores
    ('C', 'Consumo'),#Por app o por empresa
    ('V', 'Vencido'),#Al cumplirse la fecha maxima
)
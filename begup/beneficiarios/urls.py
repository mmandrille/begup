#Imports Django
from django.urls import path
from django.conf.urls import url
#Imports de la app
from . import views

app_name = 'beneficiarios'
urlpatterns = [
    #Home
    path('', views.menu, name='menu'),
    #Basicas del menu administrativo
    path('buscar/', views.buscar_beneficiario, name='buscar_beneficiario'),
    path('crear/', views.crear_beneficiario, name='crear_beneficiario'),
    path('resumen/<int:beneficiario_id>', views.resumen, name='resumen'),
    path('tarjeta/<int:beneficiario_id>', views.ver_tarjeta, name='ver_tarjeta'),
    path('modificar/<int:beneficiario_id>', views.modificar_beneficiario, name='modificar_beneficiario'),
    path('baja/<int:beneficiario_id>', views.baja_beneficiario, name='baja_beneficiario'),
    path('subir_foto/<int:beneficiario_id>', views.subir_fotografia, name='subir_fotografia'),
    path('crear_user/<int:beneficiario_id>',  views.crear_usuario_beneficiario, name='crear_usuario_beneficiario'),
    path('modpasswd/<int:user_id>',  views.mod_password_beneficiario, name='mod_password_beneficiario'),
    path('act_user/<int:user_id>',  views.activar_usuario_benef, name='activar_usuario_benef'),
    #Mod info
    path('mod_domicilios/<int:beneficiario_id>', views.modificar_domicilios, name='modificar_domicilios'),
    path('mod_educativa/<int:beneficiario_id>', views.modificar_info_educativa, name='modificar_info_educativa'),
    path('edu/historico/<int:educativa_id>', views.historico_educativo, name='historico_educativo'),
    #Manejo documentos
    path('documentos_pendientes', views.documentos_pendientes, name='documentos_pendientes'),
    path('docs/subir/<int:beneficiario_id>', views.subir_documentacion, name='subir_documentacion'),
    path('docs/activar/<int:doc_id>', views.activar_doc, name='activar_doc'),
    path('eliminar_doc/<int:doc_id>', views.eliminar_doc, name='eliminar_doc'),
    #Manejo de Creditos
    path('credito/pendientes', views.creditos_pendientes, name='creditos_pendientes'),
    path('credito/<int:credito_id>', views.ver_credito, name='ver_credito'),
    path('credito/historial/<int:beneficiario_id>', views.historial_creditos, name='historial_creditos'),
    path('credito/crear/<int:beneficiario_id>', views.crear_credito, name='crear_credito'),
    path('credito/editar/<int:credito_id>', views.editar_credito, name='editar_credito'),
    path('credito/aprobar/<int:credito_id>', views.aprobar_credito, name='aprobar_credito'),
    path('credito/eliminar/<int:credito_id>', views.baja_credito, name='eliminar_credito'),
    path('credito/baja/<int:credito_id>', views.baja_credito, name='baja_credito'),
    #Manejo de Tramos
    path('tramos/', views.lista_tramos, name='lista_tramos'),
    path('tramos/<int:tramo_id>', views.ver_tramo, name='ver_tramo'),
    path('tramos/crear/', views.crear_tramo, name='crear_tramo'),
    path('tramos/eliminar/<int:tramo_id>', views.eliminar_tramo, name='eliminar_tramo'),
    #Area de Beneficiarios
    path('pub/ingreso/', views.ingreso_beneficiario, name='ingreso_beneficiario'),
    path('pub_crear_credito/<int:beneficiario_id>', views.crear_credito, name='pub_crear_credito'),
    #path('pub_subir_documentacion/<int:beneficiario_id>', views.subir_documentacion, name='pub_subir_documentacion'),
    #path('pub_mod_beneficiario/<int:beneficiario_id>', views.modificar_beneficiario, name='pub_mod_beneficiario'),
    #path('pub_mod_domicilios/<int:beneficiario_id>', views.modificar_domicilios, name='pub_mod_domicilios'),
    #path('pub_mod_info_educativa/<int:beneficiario_id>', views.modificar_info_educativa, name='pub_mod_info_educativa'),
    #path('pub_subir_fotografia/<int:beneficiario_id>', views.subir_fotografia, name='pub_subir_fotografia'),
    #REPORTES
    path('rep/tarjetas/', views.tarjetas_impresas, name='tarjetas_impresas'),
    path('rep/creditos/', views.reporte_creditos, name='reporte_creditos'),
    url(r'down/creditos/(?P<begda>\d{4}-\d{2}-\d{2})/(?P<endda>\d{4}-\d{2}-\d{2})', views.download_creditos, name='download_creditos'),
    url(r'down/tarjetas/(?P<begda>\d{4}-\d{2}-\d{2})/(?P<endda>\d{4}-\d{2}-\d{2})', views.download_tarjetas, name='download_tarjetas'),
    #Carga Masiva
    path('upload', views.upload_beneficiarios, name='upload_beneficiarios'),
]
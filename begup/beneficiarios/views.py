#Imports de Python
import csv
import string
from datetime import date
#Imporst Extras
from auditlog.models import LogEntry
#Imports de Django
from django.utils import timezone
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import permission_required
#Imports del proyecto
from begup.constantes import NOMAIL, LAST_DATE
from georef.models import Localidad
from core.forms import UploadFotoForm
from core.forms import CrearUsuarioForm, ModUsuarioForm, ModPasswordForm
from core.forms import CrearRolForm, AuditoriaForm
from core.forms import FechaForm, PeriodoForm
from core.functions import paginador
from core.functions import instace_to_header, instance_to_line
from core.decoradores import superuser_required
from empresas.forms import CrearTramo
from empresas.models import Tramo
#Imports de la app
from .models import Beneficiario, Documentacion, Domicilio, Info_Educativa
from .models import Credito, EventoCredito
from .forms import BuscarBeneficiario, IngresoBeneficiario
from .forms import BeneficiarioForm
from .forms import DocumentacionForm, DomicilioForm, InfoEducativaForm
from .forms import CreditoForm
from .functions import get_beneficiario, obtener_eventos_diferenciados
from .decoradores import validado_simple, validado_credito, validado_educativo

# Create your views here.
#cuando estan loggeados los administradores
def menu(request):
    beneficiario =  get_beneficiario(request)
    if beneficiario:
        return render(request, 'beneficiarios/panel_publico.html', {'beneficiario': beneficiario})
    elif request.user.has_perm('beneficiarios.menu'):
        return render(request, 'administradores/menu_administradores.html', {})
    else:
        return render(request, 'extras/error.html', {})

#Ingreso publico de beneficiarios >> DEBERIAMOS EVITAR REPETIR
def ingreso_beneficiario(request):
    if request.user.is_authenticated:
        try:
            beneficiario =  Beneficiario.objects.get(usuario=request.user)
            return menu(request)
        except Beneficiario.DoesNotExist:
            return render(request, 'extras/error.html', {})
    #Si no esta loggeado, lo mandamos por aca a que se loggee
    form = IngresoBeneficiario()
    if request.method == 'POST':
        form = IngresoBeneficiario(request.POST)
        if form.is_valid():
            login(request, form.user)
            beneficiario = form.beneficiario
            return render(request, 'beneficiarios/panel_publico.html', {'beneficiario': beneficiario, })
    return render(request, "extras/generic_form.html", {'titulo': 'Ingreso de Benficiarios', 'form': form, 'boton': 'Ingresar', })

@permission_required('beneficiarios.buscar_beneficiario')
def buscar_beneficiario(request):
    form = BuscarBeneficiario()
    if request.method == 'POST':
        form = BuscarBeneficiario(request.POST)
        if form.is_valid():
            beneficiario = form.beneficiario
            return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)
    #Si todo falla o es la primera vez que entra
    return render(request, 'extras/generic_form.html', {
        'form': form, 'titulo': "Buscar Beneficiario", 'boton': "Buscar",})

def resumen(request, beneficiario_id):
    if request.user.has_perm('beneficiarios.buscar_beneficiario'):
        beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
        return render(request, 'administradores/resumen.html', {'beneficiario': beneficiario, })
    else:
        beneficiario =  get_beneficiario(request)
        if beneficiario:
            return render(request, 'beneficiarios/panel_publico.html', {'beneficiario': beneficiario})
        else:
            return render(request, 'extras/error.html', {})

@permission_required('beneficiarios.baja_beneficiario')
def baja_beneficiario(request, beneficiario_id):
    beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
    titulo = "Baja de Beneficiario"
    message = "Se procedera a dar de baja todos los creditos que tiene el beneficiario "
    message+= beneficiario.apellidos + ', ' + beneficiario.nombres
    #FALTA EL PROCESO DE BAJA
    return render(request, 'extras/confirmar.html', {
        'titulo': titulo, 
        'message': message,
        })

@permission_required('beneficiarios.ver_tarjeta')
def ver_tarjeta(request, beneficiario_id):
    beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
    return render(request, 'administradores/tarjeta.html', {'beneficiario': beneficiario, })

@permission_required('beneficiarios.crear_beneficiario')
def crear_beneficiario(request):
    form = BeneficiarioForm()
    if request.method == "POST":
        form = BeneficiarioForm(request.POST, request.FILES)
        if form.is_valid():
            beneficiario = form.save()
            return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)
    return render(request, 'extras/generic_form.html', {
        'form': form, 
        'titulo': 'Crear Beneficiario',
        'boton': 'Crear'})

@permission_required('beneficiarios.modificar_beneficiario')
def modificar_beneficiario(request, beneficiario_id):
    beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
    form = BeneficiarioForm(instance=beneficiario)
    if request.method == 'POST':
        form = BeneficiarioForm(request.POST, request.FILES, instance=beneficiario)
        if form.is_valid():
            form.save()
            return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)
    return render(request, 'extras/generic_form.html', {
        'form': form, 
        'titulo': 'Modificar Beneficiario',
        'boton': 'Modificar'})

@permission_required('beneficiarios.crear_beneficiario')
def crear_usuario_beneficiario(request, beneficiario_id):
    beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
    beneficiario.save()
    return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)

@permission_required('beneficiarios.crear_beneficiario')
def activar_usuario_benef(request, user_id):
    usuario = User.objects.get(pk=user_id)
    beneficiario = Beneficiario.objects.get(usuario=usuario)
    usuario.is_active = True
    usuario.save()
    return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)

@permission_required('beneficiarios.crear_beneficiario')
def mod_password_beneficiario(request, user_id):
    usuario = User.objects.get(pk=user_id)
    beneficiario = Beneficiario.objects.get(usuario=usuario)
    form = ModPasswordForm(initial={'username': usuario.username, })
    if request.method == 'POST':
        form = ModPasswordForm(request.POST)
        if form.is_valid():
            usuario.password = make_password(form.cleaned_data['passwd1'])
            usuario.save()
            return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)
    #Sea por ingreso o por salida:
    return render(request, "extras/generic_form.html", {'titulo': "Modificar Usuario", 'form': form, 'boton': "Modificar", })

@permission_required('beneficiarios.modificar_beneficiario')
def subir_documentacion(request, beneficiario_id):
    beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
    form = DocumentacionForm(initial={'beneficiario': beneficiario,})
    if request.method == 'POST':
        form = DocumentacionForm(request.POST, request.FILES)
        if form.is_valid():
            documento = form.save(commit=False)
            documento.beneficiario = beneficiario
            documento.fecha = date.today()
            documento.activa = False
            documento.save()
            return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)
    #Si no lo subimos o es el primer ingreso terminamos aca
    return render(request, 'extras/generic_form.html', {
        'form': form, 
        'titulo': 'Subir Documento',
        'boton': 'Subir'})

@permission_required('beneficiarios.modificar_beneficiario')
def subir_fotografia(request, beneficiario_id):
    beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
    form = UploadFotoForm(initial={'beneficiario': beneficiario,})
    if request.method == 'POST':
        form = UploadFotoForm(request.POST, request.FILES)
        if form.is_valid():
            beneficiario.fotografia = form.cleaned_data['imagen']
            beneficiario.save()
            return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)
    return render(request, 'extras/generic_form.html', {
        'form': form, 
        'titulo': 'Subir Fotografia',
        'boton': 'Subir'})

@permission_required('beneficiarios.modificar_beneficiario')
def modificar_domicilios(request, beneficiario_id):
    beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
    form = DomicilioForm(initial={'beneficiario': beneficiario})
    #Si tiene datos los precargamos
    if beneficiario.domicilio_actual():
        form = DomicilioForm(instance=beneficiario.domicilio_actual())
    #Si esta enviando datos
    if request.method == 'POST':
        form = DomicilioForm(request.POST)
        if form.is_valid():
            dom = form.save(commit=False)
            dom.id = None
            dom.fecha = timezone.now()
            dom.save()
            return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)
    return render(request, 'extras/generic_form.html', {
        'form': form, 
        'titulo': 'Modificar Domicilio',
        'boton': 'Guardar'})

@validado_educativo('beneficiarios.buscar_beneficiario')
def historico_educativo(request, educativa_id):
    info_educativa = Info_Educativa.objects.get(pk=educativa_id)
    beneficiario = info_educativa.beneficiario
    establecimiento = info_educativa.establecimiento
    historicos = Info_Educativa.objects.filter(
        beneficiario=beneficiario,
        establecimiento=info_educativa.establecimiento
    ).order_by('-fecha')

    return render(request, 'generic/historico_educativo.html', {
        'beneficiario': beneficiario,
        'establecimiento': establecimiento,
        'historicos': historicos,
    })

@permission_required('beneficiarios.modificar_beneficiario')
def modificar_info_educativa(request, beneficiario_id):
    beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
    form = InfoEducativaForm(initial={'beneficiario': beneficiario})
    #Si tiene datos educativos los precargamos
    if beneficiario.info_educativa_actual():
        form = InfoEducativaForm(instance=beneficiario.info_educativa_actual())
    #Si esta enviando datos
    if request.method == 'POST':
        form = InfoEducativaForm(request.POST)
        if form.is_valid():
            info = form.save(commit=False)
            info.id = None
            info.fecha = timezone.now()
            form.save()
            return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)
    return render(request, 'extras/generic_form.html', {
        'form': form, 
        'titulo': 'Modificar Info Educativa',
        'boton': 'Guardar'})

#Manejo de Documentacion
@permission_required('beneficiarios.ver_docs_pendientes')
def documentos_pendientes(request):
    documentos = Documentacion.objects.filter(activa=False)
    documentos = documentos.order_by('fecha')
    #Creamos paginado
    documentos = paginador(request, documentos)
    #Lanzamos
    return render(request, 'administradores/documentos_pendientes.html', {'documentos': documentos, })

@permission_required('beneficiarios.ver_docs_pendientes')
def activar_doc(request, doc_id):
    documento = Documentacion.objects.get(pk=doc_id)
    documento.activa = True#activamos
    documento.save()#guardamos
    return redirect('beneficiarios:documentos_pendientes')

@permission_required('beneficiarios.ver_docs_pendientes')
def eliminar_doc(request, doc_id):
    documento = Documentacion.objects.get(pk=doc_id)
    documento.delete()
    return redirect('beneficiarios:documentos_pendientes')

#Manejo de Creditos
@validado_credito('beneficiarios.ver_credito')
def ver_credito(request, credito_id):
    credito = Credito.objects.prefetch_related('eventos')
    credito = credito.select_related('tramo', 'tramo__origen', 'tramo__destino')
    credito = credito.get(pk=credito_id)
    beneficiario = credito.beneficiario
    #Chequeamos si se puede ver el credito o no:
    puede_ver_codigo = request.user.is_superuser#Si es superusuario
    if not puede_ver_codigo:
        if get_beneficiario(request) == credito.beneficiario:
            puede_ver_codigo = True
    #Lanzamos template
    return render(request, 'administradores/ver_credito.html', {
        'beneficiario': beneficiario, 'credito': credito,
        'puede_ver_codigo': puede_ver_codigo,
    })

@validado_simple('beneficiarios.ver_credito')
def historial_creditos(request, beneficiario_id):
    beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
    creditos = []
    for credito in beneficiario.creditos.all():
        creditos.append(credito)
    return render(request, 'administradores/historial_creditos.html', {'beneficiario': beneficiario, 'creditos': creditos, })

@permission_required('beneficiarios.ver_cred_pendientes')
def creditos_pendientes(request):
    creditos = Credito.creditos_pendientes()
    creditos = creditos.order_by('eventos__fecha')
    #Creamos paginado
    creditos = paginador(request, creditos)
    #Lanzamos
    return render(request, 'administradores/credito_pendientes.html', {'creditos': creditos, })

@validado_simple('beneficiarios.crear_credito')
def crear_credito(request, beneficiario_id):
    beneficiario = Beneficiario.objects.get(pk=beneficiario_id)
    if beneficiario.creditos.all():
        tramo_previo = beneficiario.creditos.order_by('eventos__fecha').last().tramo
    else:
        tramo_previo = None

    form = CreditoForm(initial={
        'beneficiario': beneficiario,
        'tramo': tramo_previo,
        })
    
    if request.method == 'POST':
        form = CreditoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)
    #Si entra de una o falla el form
    return render(request, 'extras/generic_form.html', {
        'form': form, 
        'titulo': 'Crear Credito',
        'boton': 'Modificar'})

@validado_credito('beneficiarios.modificar_credito')
def editar_credito(request, credito_id):
    credito = Credito.objects.get(pk=credito_id)
    form = CreditoForm(instance=credito) 
    if request.method == 'POST':
        form = CreditoForm(request.POST, instance=credito)
        if form.is_valid():
            form.save()
            return redirect('beneficiarios:resumen', beneficiario_id=credito.beneficiario.id)
    return render(request, 'extras/generic_form.html', {
        'form': form, 
        'titulo': 'Modificar Credito',
        'boton': 'Modificar'})

@permission_required('beneficiarios.aprobar_credito')
def aprobar_credito(request, credito_id):
    credito = Credito.objects.get(pk=credito_id)
    beneficiario = credito.beneficiario
    evento = EventoCredito()
    evento.credito = credito
    evento.tipo = 'A'
    evento.fecha = timezone.now()
    evento.save()
    return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)

@permission_required('beneficiarios.vencer_credito')
def baja_credito(request, credito_id):
    credito = Credito.objects.get(pk=credito_id)
    evento = EventoCredito()
    evento.credito = credito
    evento.tipo = 'V'
    evento.fecha = timezone.now()
    evento.save()
    beneficiario = credito.beneficiario
    return redirect('beneficiarios:resumen', beneficiario_id=beneficiario.id)

#Administracion de Tramos y costos
@permission_required('beneficiarios.ver_tramo')
def lista_tramos(request):
    tramos = Tramo.objects.all()
    tramos = tramos.select_related(
        'origen', 'origen__departamento',
        'destino', 'destino__departamento',
    )
    tramos = tramos.prefetch_related('costos')
    return render(request, "administradores/lista_tramos.html", {
        'tramos': tramos,
        'has_table': True,
    })

@permission_required('beneficiarios.ver_tramo')
def ver_tramo(request, tramo_id):
    tramo = Tramo.objects.get(id= tramo_id)
    costos = tramo.costos.filter(endda=LAST_DATE)
    costos = paginador(request, costos)
    return render(request, "administradores/ver_tramo.html", {'tramo': tramo, 'costos': costos, })

def crear_tramo(request):
    form = CrearTramo()
    if request.method == 'POST':
        form = CrearTramo(request.POST)
        if form.is_valid():
            form.save()
            return redirect('beneficiarios:lista_tramos')
    return render(request, "extras/generic_form.html", {'titulo': 'Crear Tramo', 'form': form, 'boton': 'Crear', })

@permission_required('beneficiarios.ver_tramo')
def eliminar_tramo(request, tramo_id):
    tramo = Tramo.objects.get(id=tramo_id)
    if not tramo.creditos.all():
        tramo.delete()
        return redirect('beneficiarios:lista_tramos')
    else:
        return render(request, 'extras/error.html', {'error': 'El Tramo indicado tiene Creditos asignados.'})

#REPORTES
@permission_required('beneficiarios.reporte_creditos')
def reporte_creditos(request):
    form = PeriodoForm()
    if request.method == "POST":
        form = PeriodoForm(request.POST)
        if form.is_valid():
            begda = form.cleaned_data['begda']
            endda = form.cleaned_data['endda']
            eventos = obtener_eventos_diferenciados(begda, endda)
            return render(request, 'administradores/reporte_creditos.html', {
                'begda': begda, 'endda': endda,
                'eventos_pedidos': eventos[0],
                'eventos_aprobados': eventos[1],
                'eventos_cobrados': eventos[2],
                'eventos_vencidos': eventos[3],
            })
    return render(request, "extras/generic_form.html", {
        'titulo': 'Informe de Creditos Gestionados', 
        'form': form, 'boton': 'Buscar', 
    })

#Generamos csv
@permission_required('beneficiarios.reporte_creditos')
def download_creditos(request, begda, endda):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="eventos_credito.csv"'
    writer = csv.writer(response)
    wr = writer.writerow
    wr(['REPORTE DE CREDITOS'])
    wr(['TIPO', 'NUM DOC', 'APELLIDO', 'NOMBRE', 'FECHA', 'CANT', 'EMPRESA'])
    eventos = EventoCredito.objects.filter(fecha__range=(begda, endda))
    eventos = eventos.select_related('credito', 'credito__beneficiario', 'consumo')
    for evento in eventos:
        try:#Si tiene los datos, escribimos la linea
            credito = evento.credito
            benef = evento.credito.beneficiario
            if evento.tipo == 'P':
                wr(['Pedido', benef.num_doc, benef.apellidos, benef.nombres, evento.fecha, credito.credito_total])      
            elif evento.tipo == 'A':
                wr(['Aprobado', benef.num_doc, benef.apellidos, benef.nombres, evento.fecha, credito.credito_total])
            elif evento.tipo == 'C':
                wr(['Aprobado', benef.num_doc, benef.apellidos, benef.nombres, evento.fecha, evento.consumo.cantidad, evento.consumo.empresa])
            elif evento.tipo == 'V':
                wr(['Vencido', benef.num_doc, benef.apellidos, benef.nombres, evento.fecha])
        except Exception as e:
            wr(['Evento '+ evento.get_tipo_display() +':', evento.id, 'Error:', e])
    #Enviamos el archivo para descargar
    return response

@permission_required('beneficiarios.reporte_tarjetas')
def tarjetas_impresas(request):
    form = PeriodoForm()
    if request.method == "POST":
        form = PeriodoForm(request.POST)
        if form.is_valid():
            begda = form.cleaned_data['begda']
            endda = form.cleaned_data['endda']
            #Obtenemos el modelo especifico
            content_type = ContentType.objects.get(app_label='beneficiarios', model="beneficiario")
            #Obtenemos los registros y los filtramos
            registros = LogEntry.objects.filter(content_type=content_type, action=1)#beneficiarios modificados
            registros = registros.filter(changes__icontains='qrpath')#Solo los que generaron qrpath
            registros = registros.filter(timestamp__range=(begda, endda))
            registros = registros.select_related('actor')
            beneficiarios = Beneficiario.objects.filter(pk__in=[r.object_id for r in registros])
            #Creamos diccionario para optimizar
            dict_beneficiarios = {}
            for benef in beneficiarios:
                dict_beneficiarios[benef.id] = benef
            return render(request, 'administradores/tarjetas_impresas.html', {
                'begda': begda, 'endda': endda,
                'registros': registros,
                'dict_beneficiarios': dict_beneficiarios, 
            })
    return render(request, "extras/generic_form.html", {'titulo': 'Informe de Tarjetas Impresas', 'form': form, 'boton': 'Buscar', })

@permission_required('beneficiarios.reporte_tarjetas')
def download_tarjetas(request, begda, endda):
    #Obtenemos el modelo especifico
    content_type = ContentType.objects.get(app_label='beneficiarios', model="beneficiario")
    #Obtenemos los registros y los filtramos
    registros = LogEntry.objects.filter(content_type=content_type, action=1)#beneficiarios modificados
    registros = registros.filter(changes__icontains='qrpath')#Solo los que generaron qrpath
    registros = registros.filter(timestamp__range=(begda, endda))
    registros = registros.select_related('actor')
    #Traemos solo los beneficiarios a los que se les imprimio tarjeta:
    beneficiarios = Beneficiario.objects.filter(pk__in=[r.object_id for r in registros])
    #Creamos diccionario para optimizar
    dict_beneficiarios = {}
    for benef in beneficiarios:
        dict_beneficiarios[benef.id] = benef
    #Iniciamos la creacion del csv
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tarjetas_impresas.csv"'
    writer = csv.writer(response)
    writer.writerow(['REPORTE DE TARJETAS IMPRESAS'])
    writer.writerow(['TIPO BENEF', 'NUM DOC', 'APELLIDO', 'NOMBRE', 'FECHA IMPRESION', 'USUARIO'])
    for registro in registros:
        beneficiario = dict_beneficiarios[registro.object_id]
        writer.writerow([
            beneficiario.get_tipo_benef_display(),
            beneficiario.num_doc,
            beneficiario.apellidos,
            beneficiario.nombres,
            registro.timestamp,
            registro.actor,
        ])
    #Enviamos el archivo para descargar
    return response

#Upload Masivo
@superuser_required
def upload_beneficiarios(request):
    titulo = "Carga Masiva de Beneficiarios"
    form = UploadCsvWithPass()
    if request.method == "POST":
        form = UploadCsvWithPass(request.POST, request.FILES)
        if form.is_valid():
            print('Iniciamos proceso de carga de Beneficiarios:')
            #Obtenemos localidades
            localidades = {l.nombre.upper():l for l in Localidad.objects.all()}
            #CARGAMOS
            file_data = form.cleaned_data['csvfile'].read().decode("utf-8")
            lines = file_data.split("\n")
            print('Obtuvimos: ', len(lines), ' lineas.')
            #0-DNI	1-NUM_BENEF  2-NOMBRE	3-APELLIDO	4-TELEFONO	5-FECHA NAC.	6-CALLE 	7-NUMERO	8-CIUDAD ORIGEN
            dict_beneficiarios = {}
            for linea in lines:
                linea = linea.split(',')
                if linea[0]:
                    benef = Beneficiario()
                    benef.num_doc = linea[0]
                    benef.num_benef = linea[1]
                    benef.nombres = string.capwords(linea[2])
                    benef.apellidos = string.capwords(linea[3])
                    benef.telefono = linea[4]
                    if len(linea[5].split('/')) == 3:
                        f = linea[5].split('/') #Viene en formato m/d/a > convertimos
                        benef.fecha_nacimiento = date(int(f[2]), int(f[0]), int(f[1]))
                    benef.email = NOMAIL
                    benef.telefono = linea[1]
                    dict_beneficiarios[benef.num_doc] = benef#No va a haber dni duplicados
            print('Filtramos a ', len(dict_beneficiarios), ' beneficiarios unicos por Numero de DNI')
            dict_beneficiarios = {b.num_benef: b for b in dict_beneficiarios.values()}#Filtramos por num de benef
            print('Filtramos a ', len(dict_beneficiarios), ' beneficiarios unicos por Numero de Beneficiario')
            #Creamos todos los beneficiarios ya limpios:
            Beneficiario.objects.bulk_create(list(dict_beneficiarios.values()))
            #Recargamos beneficiarios para que tengan id (bulkcreate no lo hace):
            dict_beneficiarios = {b.num_benef : b for b in Beneficiario.objects.all()}
            print('Recargamos: ', len(dict_beneficiarios), ' Beneficiarios.')
            #Volvemos a recorrer para crear domicilios
            domicilios = []
            for linea in lines:
                linea = linea.split(',')
                if linea[0] and ( linea[8].upper() in localidades ):
                    num_benef = int(linea[1])
                    if num_benef in dict_beneficiarios:
                        benef = dict_beneficiarios[num_benef]#Buscamos por num_benef
                        dom = Domicilio()
                        dom.beneficiario = benef
                        dom.calle = string.capwords(linea[6])
                        dom.numero = linea[7]
                        dom.localidad = localidades[linea[8].upper()]
                        domicilios.append(dom)
            #Guardamos masivamente
            print('Cargamos: ', len(domicilios), ' domicilios a los beneficiarios.')
            Domicilio.objects.bulk_create(domicilios)
            #enviamos confirmacion visual 
            return render(request, 'extras/upload_csv.html', {'count': len(lines), })
    #ingreso normal o falla en form
    return render(request, "extras/upload_csv.html", {'titulo': titulo, 'form': form, })
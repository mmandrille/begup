#Imports Python
import random
import string
from datetime import date
#Imports de Django
from django.utils import timezone
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import Group, Permission
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.contrib.auth.models import User
#imports del proyecto
from begup.constantes import NOMAIL
from empresas.functions import obtener_costo
#Imports de la app
from .models import Beneficiario
from .models import Credito, Consumo, EventoCredito
from .models import CodigoCredito

#Definimos nuestra señales
@receiver(post_save, sender=Beneficiario)
def crear_usuario(created, instance, **kwargs):
    if not instance.usuario and instance.email != NOMAIL:
        #Creamos el usuario para el beneficiario
        try:
            usuario = User.objects.get(username=str(instance.num_doc))
        except User.DoesNotExist:
            usuario = User(username=str(instance.num_doc),
                    first_name=instance.nombres,
                    last_name=instance.apellidos,
                    email=instance.email)
            usuario.save()#El mail se va a mandar automaticamente
        #Buscamos el rol basico
        try:
            grupo = Group.objects.get(name='Beneficiario_Publico')
        except Group.DoesNotExist:
             grupo = Group(name='Beneficiario_Publico')
             grupo.save()
        
        if not grupo.permissions.filter(name='beneficiarios.menu'):
            perm = Permission.objects.filter(
                content_type__app_label='beneficiarios',
                content_type__model='beneficiario').exclude(name__contains='Can ').first()
            grupo.permissions.add(perm.id)
            grupo.save()
        #Le agregamos el rol basico
        usuario.groups.add(grupo)
        usuario.save()
        #Le agregamos el usuario y guardamos
        instance.usuario = usuario
        instance.save()

@receiver(post_save, sender=Consumo)
def restar_credito(created, instance, **kwargs):
    #Automatizamos el gasto de credito por cada consumo que entra
    if created and instance.evento.tipo == 'C':
        #Obtenemos el costo en ese momento:
        instance.costo = obtener_costo(instance)
        instance.save()
        #Reducimos el credito del que se consumio
        instance.evento.credito.credito_usado+= instance.cantidad
        instance.evento.credito.save()
    

@receiver(post_save, sender=Credito)
def vencer_credito(instance, **kwargs):
    #Si se queda sin saldo vencemos el credito
    if instance.credito_total <= instance.credito_usado:
        evento = EventoCredito()
        evento.credito = instance
        evento.tipo = 'V'#vencido
        evento.fecha = timezone.now()
        evento.save()

@receiver(post_save, sender=Credito)
def crear_pedido(instance, **kwargs):
    if not instance.eventos.all():
        evento = EventoCredito()
        evento.credito = instance
        evento.tipo = 'P'
        evento.fecha = timezone.now()
        evento.save()

@receiver(post_save, sender=EventoCredito)
def generar_codigos(created, instance, **kwargs):
    if created and instance.tipo == 'A':
        #Creamos los 3 codigos, 1 de 100% y 2de 50%
        cod100 = CodigoCredito()
        cod100.credito = instance.credito
        cod100.porcentaje = 100
        cod100.clave = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
        cod100.save()
        #Primer codigo de 50%
        cod50a = CodigoCredito()
        cod50a.credito = instance.credito
        cod50a.porcentaje = 50
        cod50a.clave = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
        cod50a.save()
        #Segundo codigo de 50%
        cod50b = CodigoCredito()
        cod50b.credito = instance.credito
        cod50b.porcentaje = 50
        cod50b.clave = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
        cod50b.save()

@receiver(post_save, sender=EventoCredito)
def informar_aprobacion(created, instance, **kwargs):
    if created and instance.tipo == 'A':
        beneficiario = instance.credito.beneficiario
        if beneficiario.email != NOMAIL:#Si tiene un correo valido
            #Preparamos el correo electronico
            to_email = beneficiario.email
            mail_subject = 'Aprobacion de Credito ' + str(date.today())
            message = render_to_string('emails/aviso_aprobacion.html', {
                    'beneficiario': beneficiario,
                    'credito': instance.credito,
                })
            #Instanciamos el objeto mail con destinatario
            email = EmailMessage(mail_subject, message, to=[to_email])
            #Enviamos el correo
            email.send()

#Imports Django
from django.shortcuts import render
#Imports del proyecto
from .models import Credito, Info_Educativa
from .functions import get_beneficiario, check_beneficiario

def validado_simple(permiso_name):
    def decorador(vista):
        def funcion_decorada(request, beneficiario_id):
            if request.user.has_perm(permiso_name):
                return vista(request, beneficiario_id)
            #Chequeamos que no sea el pibe intentando acceder a su propio perfil
            elif check_beneficiario(request, beneficiario_id):
                return vista(request, beneficiario_id)
            else:
                #Si no pasa ninguno de los dos criterios:
                return render(request, 'users/no_perms.html', {})
        return funcion_decorada
    return decorador

def validado_credito(permiso_name):
    def decorador(vista):
        def funcion_decorada(request, credito_id):
            if request.user.has_perm(permiso_name):
                return vista(request, credito_id)
            #Chequeamos que no sea el pibe intentando acceder a su propio perfil
            else:
                #Obtenemos el credito con la credencial del usuario
                try:
                    credito = Credito.objects.get(pk=credito_id, beneficiario__usuario=request.user)
                    return vista(request, credito_id)
                except Credito.DoesNotExist:
                    return render(request, 'users/no_perms.html', {})
        return funcion_decorada
    return decorador

def validado_educativo(permiso_name):
    def decorador(vista):
        def funcion_decorada(request, educativa_id):
            if request.user.has_perm(permiso_name):
                return vista(request, educativa_id)
            #Chequeamos que no sea el pibe intentando acceder a su propio perfil
            else:
                #Obtenemos el credito con la credencial del usuario
                try:
                    educativa = Info_Educativa.objects.get(pk=educativa_id, beneficiario__usuario=request.user)
                    return vista(request, educativa_id)
                except Info_Educativa.DoesNotExist:
                    return render(request, 'users/no_perms.html', {})
        return funcion_decorada
    return decorador
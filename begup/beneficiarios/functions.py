#Imports Python
from datetime import timedelta
#Imports Django
from django.utils import timezone
#Imports del Proyecto

#Imports de la app
from .models import Beneficiario, EventoCredito, Consumo

#Definimos funciones
def get_beneficiario(request):
    try:
        return Beneficiario.objects.get(usuario=request.user)
    except Beneficiario.DoesNotExist:
        return None

def check_beneficiario(request, beneficiario_id):
    try:
        beneficiario = Beneficiario.objects.get(pk=beneficiario_id, usuario=request.user)
        return beneficiario
    except Beneficiario.DoesNotExist:
        return None

def boletos_vencidos(beneficiario):
    #Recorremos los creditos activos
    for credito in beneficiario.creditos_actuales():
        fecha_temp = credito.fecha_aprobado()#Obtenemos fecha de Aprobacion
        if credito.eventos.filter(tipo='C'):#Nos fijamos si no hay que adelantar por consumos
            fecha_temp = credito.eventos.filter(tipo='C').order_by('fecha').last().fecha
        else:
            fecha_temp+= timedelta(days=2)#Adelantamos dos dias de regalo
            if fecha_temp.weekday() == 0:#Si es domingo adelantamos un dia mas > Lunes
                fecha_temp+= timedelta(days=1)
            elif fecha_temp.weekday() == 6:#Si es sabado, adelantamos dos dias > Lunes
                fecha_temp+= timedelta(days=2)
        #Comenzamos la quita de pasajes
        while fecha_temp < timezone.now() and credito.credito_actual:#hasta llegar a hoy o que se quede sin credito
            if not credito.eventos.filter(fecha=fecha_temp):
                evento = EventoCredito()
                evento.credito = credito
                evento.tipo = 'C'
                evento.fecha = fecha_temp
                evento.descripcion = "Descuento por no retiro de pasajes."
                evento.save()
                consumo = Consumo()
                consumo.evento = evento
                consumo.cantidad = 2
                consumo.save()
            fecha_temp+= timedelta(days=1)

def obtener_eventos_diferenciados(begda, endda):
    eventos_pedidos = EventoCredito.objects.filter(tipo='P', fecha__range=(begda,endda))
    eventos_pedidos = eventos_pedidos.select_related(
        'credito', 'consumo',
        'credito__beneficiario',
        'credito__tramo', 'credito__tramo__origen', 'credito__tramo__destino')
    eventos_pedidos.nombre = "EVENTOS PEDIDOS"
    eventos_aprobados = EventoCredito.objects.filter(tipo='A', fecha__range=(begda,endda))
    eventos_aprobados = eventos_aprobados.select_related(
        'credito', 'consumo',
        'credito__beneficiario',
        'credito__tramo', 'credito__tramo__origen', 'credito__tramo__destino')
    eventos_aprobados.nombre = "EVENTOS APROBADOS"
    #eventos_vencidos = EventoCredito.objects.filter(tipo='V', fecha__range=(begda,endda))
    eventos_cobrados = EventoCredito.objects.filter(tipo="C", fecha__range=(begda,endda)).exclude(consumo__empresa=None)
    eventos_cobrados = eventos_cobrados.select_related(
        'credito', 'consumo', 'consumo__empresa',
        'credito__beneficiario',
        'credito__tramo', 'credito__tramo__origen', 'credito__tramo__destino')
    eventos_cobrados.nombre = "EVENTOS COBRADOS"
    eventos_vencidos = EventoCredito.objects.filter(tipo="C", fecha__range=(begda,endda), consumo__empresa=None)
    eventos_vencidos = eventos_vencidos.select_related(
        'credito', 'consumo',
        'credito__beneficiario',
        'credito__tramo', 'credito__tramo__origen', 'credito__tramo__destino')
    eventos_vencidos.nombre = "EVENTOS VENCIDOS"
    return [eventos_pedidos, eventos_aprobados, eventos_cobrados, eventos_vencidos]

#Obtenemos todas las vistas existentes en la app
def obtener_vistas():
    from .urls import urlpatterns as urls
    vistas = []
    for url in urls:
        vistas.append(url.name)
    return vistas
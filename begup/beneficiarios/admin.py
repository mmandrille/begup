#Modulos de Django
from django.db import models
from django.contrib import admin
from django.forms import TextInput, CheckboxSelectMultiple
#Modulos extras
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
#imports del proyecto
from core.admin import register_hidden_models

#Importamos nuestra app
from .models import TipoDocumento
from .models import Beneficiario, Info_Educativa, Domicilio, Documentacion
from .models import Credito, EventoCredito, CodigoCredito
from .models import Administrador

# Register your models here.
#Definimos los inlines:
class DomicilioInline(NestedStackedInline):
    model = Domicilio
    fk_name = 'beneficiario'
    autocomplete_fields = ['localidad', 'barrio']
    extra = 0
    autocomplete_fields = ('localidad', )

class InfoEducativaInline(NestedStackedInline):
    model = Info_Educativa
    fk_name = 'beneficiario'
    extra = 0
    autocomplete_fields = ('establecimiento', 'carrera')

class DocumentacionInline(NestedStackedInline):
    model = Documentacion
    fk_name = 'beneficiario'
    extra = 0

class CreditoInline(NestedStackedInline):
    model = Credito
    fk_name = 'beneficiario'
    extra = 0
    autocomplete_fields = ('tramo', )
    def has_delete_permission(self, request, obj=None):
        return False

class EventoCreditoInline(admin.TabularInline):
    model = EventoCredito
    fk_name = 'credito'
    extra = 0
    def has_delete_permission(self, request, obj=None):
        return False

class CodigoCreditoInline(admin.TabularInline):
    model = CodigoCredito
    fk_name = 'credito'
    extra = 0
    def has_delete_permission(self, request, obj=None):
        return False

#Definimos nuestros modelos administrables:
class BeneficiarioAdmin(NestedModelAdmin):#(NestedModelAdmin):
    model = Beneficiario
    search_fields = ['num_doc', 'apellidos', 'nombres']
    list_filter = ['info_educativa__nivel', 'info_educativa__grado']
    autocomplete_fields = ('usuario',)
    readonly_fields = ['num_benef', 'qrpath']
    inlines = [CreditoInline, DomicilioInline, InfoEducativaInline, ]#DocumentacionInline]
    formfield_overrides = {
        models.IntegerField: {'widget': TextInput(attrs={'size':'20'})},
    }
    fields = [('tipo_benef', 'num_benef'),
            ('tipo_doc', 'num_doc'), 
            ('nombres', 'apellidos'),
            'fecha_nacimiento',
            'telefono', 'email',
            'usuario',
            'fotografia',]
    def has_delete_permission(self, request, obj=None):
        return False

class CreditoAdmin(admin.ModelAdmin):
    model = Credito
    search_fields = ['beneficiario__num_doc', 'beneficiario__apellidos', 'beneficiario__nombres']
    readonly_fields = ['credito_actual', 'estado']
    inlines = [CodigoCreditoInline, EventoCreditoInline]
    def has_delete_permission(self, request, obj=None):
        return False

class AdministradoresAdmin(admin.ModelAdmin):
    model = Administrador
    search_fields = ['usuario__username']
    autocomplete_fields = ['usuario', ]
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

#Registramos nuestros modelos
register_hidden_models(TipoDocumento)
admin.site.register(Beneficiario, BeneficiarioAdmin)
admin.site.register(Credito, CreditoAdmin)
admin.site.register(Administrador, AdministradoresAdmin)

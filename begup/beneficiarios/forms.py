#Imports Python
from django.utils import timezone
from dateutil.relativedelta import relativedelta
#Imports Extras
from dal import autocomplete, forward
#Imports Django
from django import forms
from django.db.models import Max
from django.db.models import Q
from django.contrib.auth import authenticate
#Imports de la app
from .models import Beneficiario, Documentacion, Domicilio, Info_Educativa
from .models import Credito

#Definimos nuestros formularios
class BeneficiarioForm(forms.ModelForm):
    class Meta:
        model = Beneficiario
        fields= '__all__'
        exclude = ('usuario', 'fotografia', 'qrpath', 'domicilio_beneficiarios')
    def __init__(self, *args, **kwargs):
        super(BeneficiarioForm, self).__init__(*args, **kwargs)
        max_num = Beneficiario.objects.all().aggregate(Max('num_benef'))['num_benef__max']
        if max_num:
            self.fields['num_benef'].initial = max_num + 1
        else:
            self.fields['num_benef'].initial = 1
        self.fields['num_benef'].widget.attrs['readonly'] = True

class BuscarBeneficiario(forms.Form):
    num_benef = forms.IntegerField(label='Num de Beneficiario', required=False)
    num_doc = forms.IntegerField(label='Num de Documento', required=False)
    widgets = {
            'num_benef': forms.TextInput(attrs={}),
            'num_doc': forms.TextInput(attrs={}),
        }
    def clean(self):
        try:
            if self.cleaned_data['num_benef']:
                beneficiario = Beneficiario.objects.get(num_benef=self.cleaned_data['num_benef'])            
            if self.cleaned_data['num_doc']:
                beneficiario = Beneficiario.objects.get(num_doc=self.cleaned_data['num_doc'])
        except Beneficiario.DoesNotExist:
            raise forms.ValidationError('No existe beneficiario con estos datos.')
        
        if beneficiario:
            self.beneficiario = beneficiario
        else:
            raise forms.ValidationError('No existe beneficiario con estos datos.')
        
        return self.cleaned_data   

class DomicilioForm(forms.ModelForm):
    class Meta:
        model = Domicilio
        fields= '__all__'
        exclude = ('fecha', 'activa')
        widgets = {
            'beneficiario': forms.HiddenInput(),
            'localidad': autocomplete.ModelSelect2(url='georef:localidad-autocomplete'),
            'barrio': autocomplete.ModelSelect2(url='georef:barrio-autocomplete', forward=['localidad']),
        }

class InfoEducativaForm(forms.ModelForm):
    class Meta:
        model = Info_Educativa
        fields= '__all__'
        exclude = ('fecha', )
        widgets = {
            'beneficiario': forms.HiddenInput(),
            'establecimiento': autocomplete.ModelSelect2(url='establecimientos:establecimiento-autocomplete'),
            'carrera': autocomplete.ModelSelect2(url='establecimientos:carrera-autocomplete', forward=['establecimiento']),
        }

class DocumentacionForm(forms.ModelForm):
    class Meta:
        model = Documentacion
        fields= '__all__'
        exclude = ('fecha', 'activa')
        widgets = {
            'beneficiario': forms.HiddenInput(),
        }

#Manejo de creditos con todos los chequeos!
class CreditoForm(forms.ModelForm):
    class Meta:
        model = Credito
        fields= '__all__'
        exclude = ('credito_usado',)
        widgets = {
            'beneficiario': forms.HiddenInput(),
            'tramo': autocomplete.ModelSelect2(url='empresas:tramo-autocomplete'),
        }
    #Chequeamos la maxima cantidad de pasajes permitidos:
    def clean_credito_total(self):
        beneficiario = self.cleaned_data['beneficiario']
        credito_total = self.cleaned_data['credito_total']
        if not beneficiario.escuela_actual():
            raise forms.ValidationError("El beneficiario no tiene informacion Educativa Activa")
        max_pasajes = beneficiario.escuela_actual().max_pasajes()
        if credito_total > max_pasajes:
            raise forms.ValidationError("Puede pedir un maximo de " + str(max_pasajes) + " pasajes.")
        return self.cleaned_data['credito_total']
    #Chequeamos que no cree pasajes antes de lo permitido
    def clean(self):
        if not hasattr(self.instance, 'pk'):#Si no es una edicion controlamos
            #Controlamos que no haya pedido credito hace menos de un mes
            beneficiario = self.cleaned_data['beneficiario']
            tramo = self.cleaned_data['tramo']
            #Chequeamos que no le hayamos aprobado el mismo credito este mes
            ultimos_creditos = Credito.objects.filter(
                Q(beneficiario=beneficiario, tramo=tramo, eventos__tipo='A')
                | Q(beneficiario=beneficiario, tramo=tramo.inverso(), eventos__tipo='A')
            )
            ultimo_credito = ultimos_creditos.order_by('eventos__fecha').last()
            if ultimo_credito:#Si ha pedido credito, chequeamos 
                fecha_habilitada = ultimo_credito.eventos.get(tipo='A').fecha + relativedelta(months=1)
                #Chequeamos fecha habilitada vs hoy
                if timezone.now() <= fecha_habilitada:
                    error = "Puede pedir credito nuevamente el " + str(fecha_habilitada)
                    raise forms.ValidationError(error)
            #Chequeamos que no tenga un credito pedido para ese tramo
            #IMPORTANTE: Tenemos que chequear que no quiera editar el mismo
            ultimos_creditos = Credito.objects.filter(
                Q(beneficiario=beneficiario, tramo=tramo, eventos__tipo='P')
                | Q(beneficiario=beneficiario, tramo=tramo.inverso(), eventos__tipo='P')
            ).exclude(eventos__tipo="V")
            ultimo_credito = ultimos_creditos.order_by('eventos__fecha').last()
            if ultimo_credito:#Si tiene creditos pedidos
                raise forms.ValidationError("Ya tiene un credito pedido esperando aprobacion.")
        return self.cleaned_data

class IngresoBeneficiario(forms.Form):
    num_doc = forms.IntegerField(label='Num de Documento', required=True)
    password = forms.CharField(label="Password", max_length=20, required=True, widget=forms.PasswordInput)
    def clean(self):
        username = str(self.cleaned_data['num_doc'])
        password = self.cleaned_data['password']
        # Verificamos las credenciales del usuario
        user = authenticate(username=username, password=password)
        try:
            beneficiario = Beneficiario.objects.get(num_doc=int(username))
        except Beneficiario.DoesNotExist:
            raise forms.ValidationError("El usuario no Existe.")

        if user and beneficiario:
            self.user = user
            self.beneficiario = beneficiario
            return self.cleaned_data
        else:
            raise forms.ValidationError("No tiene permiso para acceder al sistema.")
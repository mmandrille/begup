#Imports de python
import qrcode
#Imports django
from django.db.models import Q
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User
#Imports Extras
from auditlog.registry import auditlog
#imports del proyecto
from begup.settings import DEBUG, MEDIA_ROOT, LOADDATA
from georef.models import Localidad, Barrio
from core.choices import TIPO_DOCUMENTOS
from establecimientos.models import Establecimiento, Carrera
from establecimientos.choices import NIVEL_EDUCATIVO, GRADO_EDUCATIVO
from empresas.models import Tramo, Empresa, Unidad
#Imports de la app
from .choices import TIPO_BENEFICIARIOS, TIPO_EVENTO_CREDITO

# Create your models here.
class TipoDocumento(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    class Meta:
        ordering = ('nombre',)
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            "id": self.id,
            "nombre": self.nombre,
        }

class MaxPasaje(models.Model):
    establecimiento = models.OneToOneField(Establecimiento, on_delete=models.CASCADE, related_name="maximo")
    albergue = models.BooleanField(default=False)
    credito_max = models.SmallIntegerField(verbose_name='Pasajes Maximos', default=46)
    def as_dict(self):
        return {
            "id": self.id,
            "establecimiento_id": self.establecimiento.id,
            "es_albergue": self.albergue,
            "credito_max": self.credito_max,
        }

class Beneficiario(models.Model):
    tipo_benef = models.IntegerField(choices=TIPO_BENEFICIARIOS, default=2)
    num_benef = models.IntegerField('Numero de Beneficiario', unique=True)
    tipo_doc = models.IntegerField(choices=TIPO_DOCUMENTOS, default=2)
    num_doc = models.IntegerField('Numero de Documento', unique=True)
    apellidos = models.CharField('Apellidos', max_length=100)
    nombres = models.CharField('Nombres', max_length=100)
    fecha_nacimiento = models.DateField(verbose_name="Fecha de Nacimiento")
    telefono = models.CharField('Telefono', max_length=20, default='+549388', null=True, blank=True)
    email = models.EmailField('Correo Electronico')#Enviar mails
    fotografia = models.FileField('Fotografia', upload_to='beneficiarios/foto/', null=True, blank=True)
    usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name="beneficiarios")
    qrpath = models.CharField('qrpath', max_length=100, null=True, blank=True)
    class Meta:
        permissions = (
            ("menu", "Puede Acceder al menu de Beneficiarios"),
        )
    def __str__(self):
        return self.apellidos + ' ' + self.nombres
    def as_dict(self):
        return {
            "id": self.id,
            "tipo_benef": self.get_tipo_benef_display(),
            "tipo_doc": self.get_tipo_doc_display(),
            "num_doc": self.num_doc,
            "apellidos": self.apellidos,
            "nombres": self.nombres,
            "fecha_nacimiento": str(self.fecha_nacimiento),
            "telefono": self.telefono,
            "email": self.email,
            "fotografia": str(self.fotografia),
        }
    def info_educativa_actual(self):
        return self.info_educativa.all().last()
    def escuela_actual(self):
        if self.info_educativa_actual():
            return self.info_educativa_actual().establecimiento
    def info_educativa_diferente(self):
        escuelas = {}
        for info in self.info_educativa.all().order_by('-fecha'):
            if info.establecimiento.nombre not in escuelas.keys():
                escuelas[info.establecimiento.nombre] = info
        return list(escuelas.values())
    def domicilio_actual(self):
        return self.domicilio_beneficiarios.last()
    def documentos_activos(self):
        return self.documentos.filter(activa=True)
    def creditos_pedidos(self):
        return self.creditos.filter(eventos__tipo='P').exclude(eventos__tipo='A').exclude(eventos__tipo='V')
    def creditos_actuales(self):
        return self.creditos.filter(eventos__tipo='A').exclude(eventos__tipo='V')
    def creditos_vencidos(self):
        return self.creditos.filter(eventos__tipo='V')
    def get_qrimage(self):
        if self.qrpath:
            return self.qrpath
        else:
            path = MEDIA_ROOT + '/beneficiarios/qrcode-'+ str(self.num_benef)
            img = qrcode.make(str(self.num_benef))
            img.save(path)
            relative_path = '/archivos/beneficiarios/qrcode-'+ str(self.num_benef)
            self.qrpath = relative_path
            self.save()
            return self.qrpath

class Domicilio(models.Model):
    beneficiario = models.ForeignKey(Beneficiario, on_delete=models.CASCADE, related_name="domicilio_beneficiarios")
    localidad = models.ForeignKey(Localidad, on_delete=models.CASCADE, related_name="domicilios_beneficiarios")
    barrio = models.ForeignKey(Barrio, on_delete=models.SET_NULL, null=True, blank=True, related_name="domicilios_beneficiarios")
    calle = models.CharField('Calle', max_length=50, default='', blank=False)
    numero = models.CharField('Numero', max_length=50, default='', blank=False)
    fecha = models.DateField(verbose_name="Fecha", default=timezone.now)
    class Meta:
        ordering = ('fecha',)
    def __str__(self):
        return self.calle + ' ' + self.numero + ', ' + str(self.localidad)
    def as_dict(self):
        return {
            "id": self.id,
            "beneficiario_id": self.beneficiario.id,
            "localidad": str(self.localidad),
            "barrio": str(self.barrio),
            "calle": self.calle,
            "numero": self.numero,
        }

class Info_Educativa(models.Model):
    beneficiario = models.ForeignKey(Beneficiario, on_delete=models.CASCADE, related_name="info_educativa")
    establecimiento = models.ForeignKey(Establecimiento, on_delete=models.CASCADE, related_name="alumnos")
    carrera = models.ForeignKey(Carrera, on_delete=models.CASCADE, null=True, blank=True, related_name="alumnos")
    nivel = models.IntegerField(choices=NIVEL_EDUCATIVO, default=1)
    grado = models.IntegerField(choices=GRADO_EDUCATIVO, default=1)
    materias_aprobadas = models.IntegerField('Materias Aprobadas', default=0)
    fecha = models.DateField(verbose_name="Fecha", default=timezone.now)
    class Meta:
        verbose_name = "Informacion Educativa"
        ordering = ('fecha',)
    def as_dict(self):
        return {
            "id": self.id,
            "beneficiario_id": self.beneficiario.id,
            "establecimiento": str(self.establecimiento),
            "nivel": self.get_nivel_display(),
            "grado": self.get_grado_display(),
        }

class Documentacion(models.Model):
    beneficiario = models.ForeignKey(Beneficiario, on_delete=models.CASCADE, related_name="documentos")
    tipo = models.ForeignKey(TipoDocumento, on_delete=models.CASCADE, related_name="documentos")
    archivo = models.FileField('Archivo', upload_to='beneficiarios/documentacion/')
    fecha = models.DateField(verbose_name="Fecha de Presentacion", default=timezone.now)
    activa = models.BooleanField(default=True)
    def as_dict(self):
        return {
            "beneficiario_id": self.beneficiario.id,
            "id": self.id,
            "tipo_documento": self.tipo.id,
            "fecha": str(self.fecha),
            "activa": self.activa,
        }

class Credito(models.Model):
    beneficiario = models.ForeignKey(Beneficiario, on_delete=models.SET_NULL, null=True, related_name="creditos")
    tramo = models.ForeignKey(Tramo, on_delete=models.SET_NULL, null=True, related_name="creditos")
    credito_total = models.SmallIntegerField(verbose_name='Credito Total', default=0)
    credito_usado = models.SmallIntegerField(verbose_name='Credito Usado', default=0)
    #Funciones de clase
    @classmethod
    def creditos_pendientes(cls, begda=None, endda=None):
        creditos = cls.objects.filter(eventos__tipo='P').exclude(eventos__tipo__in=('A', 'V'))
        if begda and endda:
            creditos = creditos.filter(Q(eventos__tipo='P') & Q(eventos__fecha__range=(begda, endda)))
        creditos = creditos.select_related('beneficiario')
        creditos = creditos.select_related('tramo', 'tramo__origen', 'tramo__destino')
        creditos = creditos.prefetch_related('eventos')
        return creditos
    @classmethod
    def creditos_activos(cls, begda=None, endda=None):
        creditos = cls.objects.filter(eventos__tipo='A').exclude(eventos__tipo='V')
        if begda and endda:
            creditos = creditos.filter(Q(eventos__tipo='A') & Q(eventos__fecha__range=(begda, endda)))
        creditos = creditos.select_related('beneficiario')
        creditos = creditos.select_related('tramo', 'tramo__origen', 'tramo__destino')
        creditos = creditos.prefetch_related('eventos')
        return creditos
    #Funciones de instancia
    def _get_credito_actual(self):
        return self.credito_total - self.credito_usado
    def _get_estado(self):
        if self.eventos.filter(tipo='V'):
            return 'V'
        elif self.eventos.filter(tipo='A'):
            return 'A'
        else:
            return 'P'
    #Campos dinamicos
    credito_actual = property(_get_credito_actual)
    estado = property(_get_estado)
    #Funciones utiles
    def estado_str(self):
        try:
            return self.eventos.filter(tipo=self.estado).first().get_tipo_display()
        except:
            return "Estado inaccesible"
    def __str__(self):
        return str(self.beneficiario) + '. Para Tramo: ' + str(self.tramo) + ' .Saldo: ' + str(self.credito_actual)
    def as_dict(self):
        return {
            "beneficiario_id": self.beneficiario.id,
            "id": self.id,
            "tramo": str(self.tramo),
            "credito_total": self.credito_total,
            "credito_usado": self.credito_usado,
        }    
    def fecha_pedido(self):
        evento = self.eventos.filter(tipo='P').first()
        if evento: return evento.fecha
        else: return None
    def fecha_aprobado(self):
        evento = self.eventos.filter(tipo='A').first()
        if evento: return evento.fecha
        else: return None
    def validar_codigo(self, clave):
        try:
            return self.codigos.get(clave=clave, usado=False)
        except CodigoCredito.DoesNotExist:
            return None

class EventoCredito(models.Model):
    credito = models.ForeignKey(Credito, on_delete=models.SET_NULL, null=True, related_name="eventos")
    tipo = models.CharField(max_length=1, choices=TIPO_EVENTO_CREDITO, default='P')
    fecha = models.DateTimeField(verbose_name="Fecha del Evento", default=timezone.now)
    descripcion = models.CharField(max_length=200, blank=True, null=True)
    class Meta:
        ordering = ['fecha']
    def __str__(self):
        return self.get_tipo_display() + ' el ' + str(self.fecha)
    def as_dict(self):
        if self.credito:
            credito_id = self.credito.id
        else:
            credito_id = 'Eliminado'
        return {
            "credito_id": credito_id,
            "id": self.id,
            "tipo_evento": self.get_tipo_display(),
            "fecha": str(self.fecha),
        }

class CodigoCredito(models.Model):
    credito = models.ForeignKey(Credito, on_delete=models.SET_NULL, null=True, related_name="codigos")
    porcentaje = models.SmallIntegerField('Porcentaje', default=100)
    clave = models.CharField('Clave de Retiro', max_length=6)
    usado = models.BooleanField('Utilizado', default=False)
    def __str__(self):
        return self.clave + ': ' + str(self.porcentaje) + '%'
    #No se debe publicar

class Consumo(models.Model):
    evento = models.OneToOneField(EventoCredito, on_delete=models.SET_NULL, null=True, related_name="consumo")
    empresa = models.ForeignKey(Empresa, on_delete=models.SET_NULL, null=True, related_name="consumo")
    cantidad = models.SmallIntegerField(verbose_name='Cantidad de Creditos Usados', default=1)
    costo = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    unidad = models.ForeignKey(Unidad, on_delete=models.SET_NULL, null=True, related_name="consumo")
    actualizado = models.DateTimeField(verbose_name="Fue Actualizado", null=True)
    #gps_point = GeopositionField(default='-24.185555999999956,-65.30602475')
    latitud = models.DecimalField('latitud', max_digits=12, decimal_places=10, default=-24.185555999999956)
    longitud = models.DecimalField('longitud', max_digits=12, decimal_places=10, default=-65.30602475)
    def subtotal(self):
        return self.cantidad * self.costo
    def __str__(self):
        pass
        return str(self.empresa) + ' por ' + str(self.cantidad)
    def as_dict(self):
        if self.empresa:
            empresa_id = self.empresa.id
        else:
            empresa_id = 'NODATA'
        return {
            "id": self.id,
            "empresa_id": empresa_id,
            "cantidad": self.cantidad,
        }

class Administrador(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, related_name="administradores_beneficiario")
    class Meta:
        verbose_name_plural = "Administradores"
        permissions = (
            ("menu", "Puede Acceder al menu de Administradores"),
            ("buscar_beneficiario", "Puede Buscar Beneficiarios"),
            ("crear_beneficiario", "Puede Crear Beneficiarios"),
            ("modificar_beneficiario", "Puede Modificar los datos de los beneficiarios"),
            ("modificar_password", "Puede cambiar password de los beneficiarios"),
            ("baja_beneficiario", "Puede dar de baja a los beneficiarios"),
            ("ver_tarjeta", "Puede ver e imprimir Tarjetas"),
            
            ("crear_credito", "Puede Crear Pedido de Credito"),
            ("ver_credito", "Ver Credito"),
            ("modificar_credito", "Puede Modificar Credito"),
            ("aprobar_credito", "Puede Aprobar Credito"),
            ("eliminar_credito", "Eliminar Credito sin Aprobar"),
            ("vencer_credito", "Vencer Credito Aprobado"),

            ("ver_reporte_diario_personal", "Puede Ver su Reporte Diario"),
            ("ver_reporte_diario", "Puede Ver el Reporte Diario General"),
            ("ver_facturacion", "Puede Ver el Reportes de Facturacion"),

            ("ver_docs_pendientes", "Puede Ver Documentos esperando Aprobacion"),
            ("ver_cred_pendientes", "Puede Ver Creditos esperando Aprobacion"),

            ("ver_tramo", "Puede Administrar Tramos/Recorridos."),
            ("ver_costos", "Puede Ver costos de las empresas."),
            ("auditaria_propia", "Puede ver SU registro de auditoria."),
            ("auditar_operadores", "Puede Auditar Acciones de los Operadores"),
            ("informe_diario", "Puede Ver el informe diario de las empresas."),
            ("facturacion_mensual", "Puede Ver la Facturacion Mensual de las empresas."),
            ("facturacion_admin", "Puede Administrar la Facturacion de las empresas."),
            ("reporte_tarjetas", 'Puede ver el Informe de Tarjetas Impresas'),
            ("reporte_creditos", 'Puede ver el Informe de Creditos Gestionados'),
            ("administrador", "Puede administrar Usuarios."),
        )
    def __str__(self):
        return self.usuario.username
    def as_dict(self):
        return {
            "id": self.id,
            "usuario": self.usuario.username,
            "grupos": [g.name for g in self.usuario.groups.all()],
        }
    @classmethod
    def has_administrado(cls):
        return False
    def get_administrado(self):
        return 'Beneficiarios'

#Auditoria
auditlog.register(MaxPasaje)
auditlog.register(Beneficiario)
auditlog.register(Domicilio)
auditlog.register(Info_Educativa)
auditlog.register(Documentacion)
auditlog.register(Credito)
auditlog.register(EventoCredito)
auditlog.register(Consumo)
auditlog.register(Administrador)

#Definimos nuestra señales:
if not LOADDATA:
    from .signals import restar_credito#Automatizamos el gasto de credito por cada consumo que entra
    from .signals import vencer_credito#Automatizamos el Vencimiento de credito al quedarse sin saldo
    from .signals import crear_pedido#Automatizamos la creacion del primer evento: Pedido
    from .signals import generar_codigos#Automatizamos la generacion de los 3 codigos al aprobar credito
    if not DEBUG:
        from .signals import informar_aprobacion#Manda mail al beneficiario
#Imports Python
from datetime import date
#Imports Django
from django.utils import timezone
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.forms.widgets import CheckboxSelectMultiple
#Imports extra
from dal import autocomplete
#Imports del proyecto
from begup.settings import SECRET_KEY
#Imports de la app
from .models import Consulta
from .widgets import XDSoftDatePickerInput, XDSoftDateTimePickerInput

#Definimos nuestros formularios
class ConsultaForm(forms.ModelForm):
    class Meta:
        model = Consulta
        fields = ['autor', 'email', 'telefono', 'asunto', 'descripcion']
        widgets = {
            'descripcion': forms.Textarea(attrs={'cols': 40, 'rows': 10}),
        }

class UploadCsv(forms.Form):
    csvfile = forms.FileField(widget=forms.FileInput(attrs={'accept': ".csv"}))

class UploadFotoForm(forms.Form):
    imagen = forms.ImageField()

class UploadCsvWithPass(forms.Form):
    csvfile = forms.FileField(label="Archivo CSV Masivo", widget=forms.FileInput(attrs={'accept': ".csv"}))
    passwd = forms.CharField(label="Password de Administrador", max_length=100, widget=forms.PasswordInput)
    def clean(self):
        if self.cleaned_data['passwd'] == SECRET_KEY:
            return self.cleaned_data
        else:
            raise forms.ValidationError("La contraseña ingresada es incorrecta.")

class UploadDobleCsvWithPass(forms.Form):
    csvfile1 = forms.FileField(label="Archivo CSV Masivo", widget=forms.FileInput(attrs={'accept': ".csv"}))
    csvfile2 = forms.FileField(label="Archivo CSV Secundario", widget=forms.FileInput(attrs={'accept': ".csv"}))
    passwd = forms.CharField(label="Password de Administrador", max_length=100, widget=forms.PasswordInput)
    def clean(self):
        if self.cleaned_data['passwd'] == SECRET_KEY:
            return self.cleaned_data
        else:
            raise forms.ValidationError("La contraseña ingresada es incorrecta.")

class MesForm(forms.Form):
    month = forms.ChoiceField(label='Periodo', choices=[(m,m) for m in range(1,13)], initial=timezone.now().month)
    year = forms.ChoiceField(label='Año', choices=[(y,y) for y in range(2020, timezone.now().year+1)], initial=timezone.now().year)
    def __init__(self, *args, **kwargs):
        self.alinear = [('month', 'year'), ]
        super(MesForm, self).__init__(*args, **kwargs)

class FechaForm(forms.Form):
    fecha = forms.DateField(label='Fecha', initial=timezone.now(), widget=XDSoftDatePickerInput(attrs={'autocomplete':'off'}))
    def __init__(self,*args, **kwargs):
        super(FechaForm, self).__init__(*args, **kwargs)
        self.fields['fecha'].initial = timezone.now()
    def clean_fecha(self):
        if self.cleaned_data['fecha'] > date.today():
            raise forms.ValidationError("No puede ingresar una fecha posteriores a hoy.")
        else:
            return self.cleaned_data['fecha']

class PeriodoForm(forms.Form):
    begda = forms.DateField(label='Inicio', initial=timezone.now(), widget=XDSoftDatePickerInput(attrs={'autocomplete':'off'}))
    endda = forms.DateField(label='Fin', initial=timezone.now(), widget=XDSoftDatePickerInput(attrs={'autocomplete':'off'}))
    def clean(self):
        if self.cleaned_data['begda'] > self.cleaned_data['endda']:
            raise forms.ValidationError("La fecha de Inicio debe ser Menor a la de fin.")
        self.begda = self.cleaned_data['begda']
        self.endda = self.cleaned_data['endda']
        return self.cleaned_data

#Administracion generica de Usuarios
class CrearUsuarioForm(forms.Form):
    username = forms.CharField(label='Usuario', max_length=15, min_length=6)
    nombre = forms.CharField(label='Nombre', max_length=50)
    apellido = forms.CharField(label='Apellido', max_length=50)
    email = forms.EmailField(label='Email', max_length=50)
    grupos = forms.MultipleChoiceField(
        label='Grupos',
        widget=CheckboxSelectMultiple(attrs={'class':'multiplechoice',}), 
    )
    def __init__(self, *args, **kwargs):
        group_list = kwargs.pop('group_list', None)
        if group_list:
            self.base_fields['grupos'].choices = group_list.values_list('id', 'name')
        super(CrearUsuarioForm, self).__init__(*args, **kwargs)
    def clean_username(self):
        if not hasattr(self, 'instance') and User.objects.filter(username=self.cleaned_data['username']):
            raise forms.ValidationError("El usuario indicado ya esta en uso, si el usuario tiene mas de una funcion, contacte al administrador")
        return self.cleaned_data['username']
    def clean_email(self):
        if not hasattr(self, 'instance') and User.objects.filter(email=self.cleaned_data['email']):
            raise forms.ValidationError("El mail indicado ya esta en uso, si el usuario tiene mas de una funcion, contacte al administrador")
        return self.cleaned_data['email']

class ModUsuarioForm(forms.Form):
    username = forms.CharField(label='Usuario', widget=forms.TextInput(attrs={'readonly':'readonly'}))
    email = forms.EmailField(label='Email', max_length=30)
    grupos = forms.MultipleChoiceField(
        label='Grupos',
        widget=CheckboxSelectMultiple(attrs={'class':'multiplechoice',}), 
    )
    def __init__(self, *args, **kwargs):
        group_list = kwargs.pop('group_list', None)
        if group_list:
            self.base_fields['grupos'].choices = group_list.values_list('id', 'name')
        super(ModUsuarioForm, self).__init__(*args, **kwargs)
    def clean_email(self):
        if User.objects.filter(email=self.cleaned_data['email']).exclude(username=self.cleaned_data['username']):
            raise forms.ValidationError("El mail indicado ya esta usado por otro usuario, si el usuario tiene mas de una funcion, contacte al administrador")
        return self.cleaned_data['email']

class ModPasswordForm(forms.Form):
    username = forms.CharField(label='Usuario', widget=forms.TextInput(attrs={'readonly':'readonly'}))
    passwd1 = forms.CharField(label="Password", max_length=32, widget=forms.PasswordInput)
    passwd2 = forms.CharField(label="Repetir Password", max_length=32, widget=forms.PasswordInput)
    def clean_passwd2(self):
        passwd1 = self.cleaned_data['passwd1']
        passwd2 = self.cleaned_data['passwd2']
        if passwd1 != passwd2:
            raise forms.ValidationError("Las contraseñas no son iguales")
        return ''

class CrearRolForm(forms.Form):
    app_name = forms.CharField(label='Modulo', widget=forms.TextInput(attrs={'readonly':'readonly'}))
    nombre = forms.CharField(label='Nombre del Rol')
    def clean(self):
        app_name = self.cleaned_data['app_name']
        nombre = self.cleaned_data['nombre']
        self.group_name = app_name + '-' + nombre
        if Group.objects.filter(name=self.group_name):
            raise forms.ValidationError("Ya existe un Rol con ese nombre")
        return self.cleaned_data

class AuditoriaForm(forms.Form):
    usuario = forms.ModelChoiceField(
        label='Usuario',
        queryset=User.objects.all(),
        widget=autocomplete.ModelSelect2(url='core:usuarios-autocomplete'),
        required=True)
    begda = forms.DateField(label='Inicio', initial=timezone.now(), widget=XDSoftDatePickerInput(attrs={'autocomplete':'off'}))
    endda = forms.DateField(label='Fin', initial=timezone.now(), widget=XDSoftDatePickerInput(attrs={'autocomplete':'off'}))
    def __init__(self, *args, **kwargs):
        super(AuditoriaForm, self).__init__(*args, **kwargs)
        if kwargs.get('initial', None):
            if kwargs['initial'].get('usuario'):
                self.fields['usuario'].widget = forms.HiddenInput()
    def clean(self):
        if self.cleaned_data['begda'] > self.cleaned_data['endda']:
            raise forms.ValidationError("La fecha de Inicio debe ser Menor a la de fin.")
        if self.cleaned_data['endda'] > date.today():
            raise forms.ValidationError("No puede ingresar una fecha posteriores a hoy.")
        return self.cleaned_data
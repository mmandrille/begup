#Imports de Django
from django.conf.urls import url
from django.urls import path
#Imports de la app
from . import views
from . import autocomplete

#Definimos nuestros Paths
app_name = 'core'
urlpatterns = [
    #Home
    url(r'^$', views.home, name='home'),
    path('faqs', views.faqs, name='faqs'),
    path('contacto', views.contacto, name='contacto'),
    
    #Acceso de usuarios
    path('login', views.home_login, name='home_login'),
    path('logout', views.home_logout, name='home_logout'),
    
    #Administracion de usuarios
    path('user/admin_usuarios/<str:app_name>', views.admin_usuario, name='admin_usuario'),
    path('user/crear_usuario/<str:app_name>', views.crear_usuario, name='crear_usuario'),
    path('user/mod_usuario/<str:app_name>/<int:admin_id>', views.modificar_usuario, name='modificar_usuario'),
    path('user/mod_password/<str:app_name>/<int:user_id>', views.cambiar_password, name='cambiar_password'),
    path('user/des_user/<str:app_name>/<int:user_id>', views.desactivar_usuario, name='desactivar_usuario'),
    path('user/act_user/<str:app_name>/<int:user_id>', views.activar_usuario, name='activar_usuario'),
    
    #Administrar Roles
    path('grupos/<str:app_name>', views.configurar_grupos, name='configurar_grupos'),
    path('grupos/<str:app_name>/add', views.crear_grupo, name='crear_grupo'),
    path('grupos/<str:app_name>/delete/<int:grupo_id>', views.delete_grupo, name='delete_grupo'),

    #Auditoria
    path('auditoria', views.auditoria, name='auditoria'),
    path('auditoria/<int:user_id>', views.auditoria, name='auditoria_propia'),
    path('auditar/obj/<int:content_id>/<int:object_id>', views.auditar_cambios, name='auditar_cambios'),

    #Validaciones:
    url(r'^act_usuario/(?P<usuario_id>[0-9]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activar_usuario_mail, name='activar_usuario_mail'),
    url(r'^act_consulta/(?P<consulta_id>[0-9]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activar_consulta, name='activar_consulta'),
    
    #Autocomplete
    url(r'^usuarios-autocomplete/$', autocomplete.UsuariosAutocomplete.as_view(), name='usuarios-autocomplete',),

    #Web Services
    path('ws/', views.ws, name='ws'),
    path('ws/<str:nombre_app>/<str:nombre_modelo>/', views.ws, name='ws'),
]
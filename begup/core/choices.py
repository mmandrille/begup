#Choice Fields
TIPO_DOCUMENTOS = (
    (1, 'CI'),
    (2, 'DNI'),
    (3, 'LC'),
    (4, 'LE'),
    (5, 'Pasaporte'),
)

TIPO_LOG = (
    ('C', 'Creacion'),
    ('M', 'Modificacion'),
    ('E', 'Eliminacion'),       
)

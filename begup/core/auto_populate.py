#Script para cargar tablas iniciales
from begup.settings import SECRET_KEY

def dias(passwd):
    if passwd == SECRET_KEY:
        from empresas.models import Dia
        Dia.objects.all().delete()
        Dia(orden=1, nombre="Lunes").save()
        Dia(orden=2, nombre="Martes").save()
        Dia(orden=3, nombre="Miercoles").save()
        Dia(orden=4, nombre="Jueves").save()
        Dia(orden=5, nombre="Viernes").save()
        Dia(orden=6, nombre="Sabado").save()
        Dia(orden=0, nombre="Domingo").save()
        Dia(orden=9, nombre="Feriados").save()
        print("Creamos opciones de frecuencias")

def parajes(passwd):
    if passwd == SECRET_KEY:
        from georef.models import Localidad
        from empresas.models import Paraje
        Paraje.objects.all().delete()
        for l in Localidad.objects.all():
            Paraje(departamento=l.departamento, nombre=l.nombre).save()
        print("Creamos " + str(Localidad.objects.all().count()) + ' parajes a partir de las localidades')

def edu_types(passwd):
    if passwd == SECRET_KEY:
        from establecimientos.models import Edu_Comun, Edu_Especial, Edu_Adultos
        Edu_Comun.objects.all().delete()
        Edu_Comun(orden=1, nombre="Jardin Maternal").save()
        Edu_Comun(orden=2, nombre="Nivel Inicial").save()
        Edu_Comun(orden=3, nombre="Primario").save()
        Edu_Comun(orden=4, nombre="Secundaria").save()
        Edu_Comun(orden=5, nombre="Polimodal").save()
        Edu_Comun(orden=6, nombre="Educacion Superior No Universitaria(SNU)").save()
        Edu_Comun(orden=7, nombre="Ciclos de Enseñanza Artística").save()
        Edu_Comun(orden=8, nombre="Cursos y Talleres de Artística").save()
        Edu_Comun(orden=9, nombre="Trayecto Artístico Profesional").save()
        Edu_Comun(orden=10, nombre="Trayecto Técnico Profesional").save()
        Edu_Comun(orden=11, nombre="Servicios Complementarios").save()
        Edu_Comun(orden=12, nombre="Itinerario Formativo").save()
        Edu_Comun(orden=13, nombre="Cursos de Capacitación de SNU").save()
        Edu_Comun(orden=14, nombre="Educacion Superior Universitaria(SU)").save()
        Edu_Especial.objects.all().delete()
        Edu_Especial(orden=1, nombre="Jardin Maternal").save()
        Edu_Especial(orden=2, nombre="Nivel Inicial").save()
        Edu_Especial(orden=3, nombre="Primario").save()
        Edu_Especial(orden=4, nombre="Secundario").save()
        Edu_Especial(orden=5, nombre="Integracion").save()
        Edu_Especial(orden=6, nombre="Taller Primario").save()
        Edu_Especial(orden=7, nombre="Taller Secundario").save()
        Edu_Adultos.objects.all().delete()
        Edu_Adultos(orden=1, nombre="Primario").save()
        Edu_Adultos(orden=2, nombre="Secundario").save()
        Edu_Adultos(orden=3, nombre="Formacion Profesional").save()
        print("Creamos todos los tipos de establecimientos educativos")

def tipos_documentacion(passwd):
    if passwd == SECRET_KEY:
        from beneficiarios.models import TipoDocumento
        TipoDocumento.objects.all().delete()
        TipoDocumento(nombre="Documento de Identidad").save()
        TipoDocumento(nombre="Certificado de alumno regular").save()
        TipoDocumento(nombre="Partida de Nacimiento").save()
        TipoDocumento(nombre="Carnet de Vacunas").save()
        print("Creamos todos los tipos de documentos iniciales")

#Roles:
def permisos(passwd):
    if passwd == SECRET_KEY:
        from django.contrib.auth.models import Group, Permission
        Group.objects.all().delete()
        #Vamos de definir el permiso publico
        grupo = Group(name='Beneficiario_Publico')
        grupo.save()
        perm = Permission.objects.filter(
            content_type__app_label='beneficiarios',
            content_type__model='beneficiario').exclude(name__contains='Can ').first()
        grupo.permissions.add(perm.id)
        grupo.save()
        #Iniciamos creacion de roles y permisos para Beneficiarios
        Group(name='Beneficiarios-Cargador').save()
        Group(name='Beneficiarios-Avanzado').save()
        Group(name='Beneficiarios-Administrador').save()
        #Iniciamos creacion de roles y permisos para Empresas
        Group(name='Empresas-Pasajes').save()
        Group(name='Empresas-Contador').save()
        Group(name='Empresas-Administrador').save()
        #Iniciamos creacion de roles y permisos para Establecimientos
        Group(name='Establecimientos-Profesor').save()
        Group(name='Establecimientos-Directivo').save()
        Group(name='Establecimientos-Administrador').save()
        #Lanzamos creaciones masivas
        print("Creamos Grupos basicos")

def cargar_tablas(passwd):
    if passwd == SECRET_KEY:
        dias(passwd)
        parajes(passwd)
        edu_types(passwd)
        tipos_documentacion(passwd)
        permisos(passwd)
        print("Proceso de poblacion de tablas terminado")
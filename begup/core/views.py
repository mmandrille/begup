#
#       HACER PRIORIDADES
#

#Un sistema de turno por día con un máximo de 360 turnos automáticamente repartidos en los horarios de 7:00 a 19:00

# modelo de feriados > restar a maximo de pasajes


#ARREGLAR:
# SISTEMA DE DESCUENTO DE PASAJES ES SUPER LENTO!!!

#Import Python Standard
import json
#Imports de Django
from django.apps import apps
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.contrib.auth import login, logout
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.auth.hashers import make_password
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.views.decorators import staff_member_required
#Imports extras
from auditlog.models import LogEntry
#Imports de la app
from .models import Faq, Consulta
from .forms import ConsultaForm
from .forms import CrearUsuarioForm, ModUsuarioForm, ModPasswordForm, CrearRolForm, AuditoriaForm
from .functions import obtener_modeladmin, obtener_grupos, obtener_permisos, auditar_objeto
from .tokens import account_activation_token
from .decoradores import superuser_required, generic_permission_required

#Definimos las vistas
#PUBLICAS:
def home(request):
    return render(request, 'home.html', {})

def faqs(request):
    faqs_list = Faq.objects.all().order_by('orden')
    return render(request, 'faqs.html', {'faqs': faqs_list, })

def contacto(request):
    if request.method == 'POST': #En caso de que se haya realizado una busqueda
        consulta_form = ConsultaForm(request.POST)
        if consulta_form.is_valid():
            consulta = consulta_form.save()
            #enviar email de validacion
            to_email = consulta_form.cleaned_data.get('email')#Obtenemos el correo
            #Preparamos el correo electronico
            mail_subject = 'Confirma tu correo de respuesta por la Consultas Realizada al begup.'
            message = render_to_string('emails/acc_active_consulta.html', {
                    'consulta': consulta,
                    'token':account_activation_token.make_token(consulta),
                })
            #Instanciamos el objeto mail con destinatario
            email = EmailMessage(mail_subject, message, to=[to_email])
            #Enviamos el correo
            #email.send()
            return render(request, 'contacto.html', {})
    else:
        consulta_form = ConsultaForm()
    return render(request, 'contacto.html', {"form": consulta_form,
                'titulo': "Envianos una consulta:", 'boton': "Enviar"})

#Manejo de sesiones de Usuarios
def home_login(request):
    message = ''
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return home(request)
    return render(request, "extras/generic_form.html", {'titulo': "Ingresar al Sistema", 'form': form, 'boton': "Ingresar", 'message': message, })

def home_logout(request):
    logout(request)
    return home(request)

#Sistema de manejo de Usuarios No staff
@generic_permission_required('administrador')
def admin_usuario(request, app_name):
    ModelAdmin = obtener_modeladmin(app_name)
    administradores = ModelAdmin.objects.all()
    #Casos particulares:
    if not request.user.is_superuser:
        if ModelAdmin.has_administrado():
            administrador = ModelAdmin.objects.get(usuario=request.user)
            administrado = ModelAdmin.get_administrado(administrador)
            administradores = ModelAdmin.filtrar(administradores, administrado)
    #Mandamos frula
    return render(request, "users/lista_usuarios.html", {
        'administradores': administradores, 
        'app_name': app_name,
        'administrado': ModelAdmin.has_administrado(),
        'has_table': True,
    })

@generic_permission_required('administrador')
def crear_usuario(request, app_name):
    ModelAdmin = obtener_modeladmin(app_name)
    if ModelAdmin.has_administrado():
        administrador = ModelAdmin.objects.get(usuario=request.user)
        administrado = ModelAdmin.get_administrado(administrador)
        if request.user.is_superuser:
            return redirect('/'+app_name.lower()+'/crear_admin')
        elif not administrado:
            return render(request, "extras/error.html", {'error': 'No se identifico asignacion del Usuario Creador'})
    #Comenzamos el proceso de creacion:
    form = CrearUsuarioForm(group_list=obtener_grupos(app_name))#Asignamos grupos especificos de la app
    if request.method == 'POST':
        form = CrearUsuarioForm(request.POST)
        if form.is_valid():
            #Primero creamos el usuario
            usuario = User(
                username = form.cleaned_data['username'],
                #password > Se genera en el envio del mail
                email = form.cleaned_data['email'],
                first_name = form.cleaned_data['nombre'],
                last_name = form.cleaned_data['apellido'],
            )
            usuario.save()
            #Le agregamos los roles elegidos:
            for grupo in request.POST.getlist('grupos'):
                usuario.groups.add(grupo)
            usuario.save()
            #Le asignamos organizacion del creador
            administrador = ModelAdmin()
            administrador.usuario = usuario
            if ModelAdmin.has_administrado():
                administrador.set_administrado(administrado)
            administrador.save()
            #Le agregamos los permisos
            return redirect('core:admin_usuario', app_name=app_name)
    return render(request, "extras/generic_form.html", {'titulo': "Crear Usuario:", 'form': form, 'boton': "Crear"})

@generic_permission_required('administrador')
def modificar_usuario(request, app_name, admin_id):
    grupos = obtener_grupos(app_name)
    ModelAdmin = obtener_modeladmin(app_name)
    administrador = ModelAdmin.objects.get(pk=admin_id)
    form = ModUsuarioForm(group_list=grupos,
        initial={
            'username': administrador.usuario.username,
            'email': administrador.usuario.email,
            'grupos': [g.id for g in administrador.usuario.groups.all()],
        })
    if request.method == 'POST':
        form = ModUsuarioForm(request.POST)
        if form.is_valid():
            administrador.usuario.email = form.cleaned_data['email']
            administrador.save()
            #Limpiamos roles para cargar los nuevos
            for grupo in grupos:
                administrador.usuario.groups.remove(grupo.id)
            for grupo in request.POST.getlist('grupos'):
                administrador.usuario.groups.add(grupo)
            administrador.usuario.save()
            return redirect('core:admin_usuario', app_name=app_name)
    #Sea por ingreso o por salida:
    return render(request, "extras/generic_form.html", {'titulo': "Modificar Usuario", 
        'form': form, 'boton': "Modificar",})

@generic_permission_required('administrador')
def cambiar_password(request, app_name, user_id):
    usuario = User.objects.get(pk=user_id)
    form = ModPasswordForm(initial={'username': usuario.username, })
    if request.method == 'POST':
        form = ModPasswordForm(request.POST)
        if form.is_valid():
            usuario.password = make_password(form.cleaned_data['passwd1'])
            usuario.save()
            return admin_usuario(request, app_name)
    #Sea por ingreso o por salida:
    return render(request, "extras/generic_form.html", {'titulo': "Modificar Usuario", 'form': form, 'boton': "Modificar", })

@generic_permission_required('administrador')
def desactivar_usuario(request, app_name, user_id):
    user = User.objects.get(pk=user_id)
    user.is_active = False
    user.save()
    return admin_usuario(request, app_name)

@generic_permission_required('administrador')
def activar_usuario(request, app_name, user_id):
    user = User.objects.get(pk=user_id)
    user.is_active = True
    user.save()
    return admin_usuario(request, app_name)

#Manejo de roles
@superuser_required
def configurar_grupos(request, app_name):
    grupos = obtener_grupos(app_name)
    permisos = obtener_permisos(app_name)
    if request.method == 'POST':
        for grupo in grupos:
            grupo.permissions.clear()
            for permiso in permisos:
                checkbox = str(grupo.id) + '-' + str(permiso.id)
                if checkbox in request.POST:
                    grupo.permissions.add(permiso)
            grupo.save()
    return render(request, "users/configurar_roles.html", {'grupos': grupos, 'permisos': permisos, 'app_name': app_name, })

@superuser_required
def crear_grupo(request, app_name):
    form = CrearRolForm(initial={'app_name': app_name, })
    if request.method == 'POST':
        form = CrearRolForm(request.POST)
        if form.is_valid():
            Group(name=form.group_name).save()
            return redirect('/grupos/'+app_name)
    return render(request, "extras/generic_form.html", {'titulo': "Crear Grupo de Permisos", 'form': form, 'boton': "Crear", })

@superuser_required
def delete_grupo(request, app_name, grupo_id):
    Group.objects.get(id=grupo_id).delete()
    return configurar_grupos(request, app_name)

#Auditoria
@staff_member_required
def auditoria(request, user_id=None):
    form = AuditoriaForm()
    if user_id:
        usuario = User.objects.get(id=user_id)
        form = AuditoriaForm(initial={'usuario': usuario, })
    if request.method == "POST":
        form = AuditoriaForm(request.POST)
        if form.is_valid():
            begda = form.cleaned_data['begda']
            endda = form.cleaned_data['endda']
            usuario = form.cleaned_data['usuario']
            #Obtenemos los registros y los filtramos
            registros = LogEntry.objects.filter(actor=usuario)#beneficiarios modificados
            registros = registros.filter(timestamp__range=(begda, endda))
            registros = registros.select_related('content_type')
            return render(request, "extras/auditoria.html", {
                'usuario': usuario,
                'begda': begda, 'endda': endda,
                'registros': registros,})
    #Lanzamos form basico
    return render(request, "extras/generic_form.html", {'titulo': "Auditoria Usuario", 'form': form, 'boton': "Auditar", })

@staff_member_required
def auditar_cambios(request, content_id, object_id):
    #Obtenemos modelo
    modelo = ContentType.objects.get(pk=content_id).model_class()
    #Obtenemos Objeto a auditar
    objeto = modelo.objects.get(pk=object_id)
    #Obtenemos registros de cambios
    registros = auditar_objeto(objeto)
    #Lanzamos muestra:
    return render(request, 'extras/auditar_objeto.html', {
        'objeto': objeto,
        'registros': registros,
    })

#Validar correos:
def activar_usuario_mail(request, usuario_id, token):
    try:
        usuario = User.objects.get(pk=usuario_id)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        usuario = None
    if usuario and account_activation_token.check_token(usuario, token):
        usuario.is_active = True
        usuario.save()
        texto = 'Excelente! Su correo electronico fue validada.'
    else:
        texto = 'El link de activacion es invalido!'
    return render(request, 'extras/resultado.html', {'texto': texto, })

def activar_consulta(request, consulta_id, token):
    try:
        consulta = Consulta.objects.get(pk=consulta_id)
    except(TypeError, ValueError, OverflowError, Consulta.DoesNotExist):
        consulta = None
    if consulta and account_activation_token.check_token(consulta, token):
        consulta.valida = True
        consulta.save()
        texto = 'Excelente! Su correo electronico fue validada.'
    else:
        texto = 'El link de activacion es invalido!'
    return render(request, 'extras/resultado.html', {'texto': texto, })

#Web Servis generico para todas las apps del sistema
#Si el modelo tiene as_dict, aparece.
@superuser_required
def ws(request, nombre_app=None, nombre_modelo=None):
    if nombre_app and nombre_modelo:
        if apps.all_models[nombre_app][nombre_modelo.lower()]:
            modelo = apps.all_models[nombre_app][nombre_modelo.lower()]
            if hasattr(modelo, 'as_dict'):
                datos = modelo.objects.all()
                datos = [d.as_dict() for d in datos]
                return HttpResponse(json.dumps({nombre_modelo+'s': datos, "cant_registros": len(datos),}), content_type='application/json')

    apps_listas = {}
    for app, models in apps.all_models.items():
        for model in models.values():
            if hasattr(model, 'as_dict'):
                if app in apps_listas:
                    apps_listas[app].append(model._meta.model_name)
                else:
                    apps_listas[app] = [model._meta.model_name, ]
    return render(request, 'ws.html', {"apps_listas": apps_listas,})
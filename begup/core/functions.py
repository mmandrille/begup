#Imports de django
from django.contrib.auth.models import Group, Permission
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
#Imports de la app
from .apps import CoreConfig

def obtener_modeladmin(app_name):
    return CoreConfig.ADMIN_MODELS[app_name]

def obtener_grupos(app_name):
    return Group.objects.filter(name__icontains=app_name)

def obtener_permisos(app_name):
    return Permission.objects.filter(
        content_type__app_label=app_name.lower(),
        content_type__model='administrador').exclude(name__contains='Can ').order_by('id')

def obtener_administrado(AdminModel, request):
    try:
        return AdminModel.objects.get(usuario=request.user).get_administrado()
    except AdminModel.DoesNotExist:
        return None

def paginador(request, queryset):
    page = request.GET.get('page', 1)
    paginator = Paginator(queryset, 50)
    try:
        return paginator.page(page)
    except PageNotAnInteger:
        return paginator.page(1)
    except EmptyPage:
        return paginator.page(paginator.num_pages)

def instace_to_header(instance):
    linea = []
    for f in instance._meta.fields:
        linea.append(f.name,)
    return linea

def instance_to_line(instance):
    linea = []
    for f in instance._meta.fields:
        if hasattr(instance, f.name):
            linea.append(str(getattr(instance, f.name)),)
        else:
            linea.append('',)
    return linea

def auditar_objeto(instancia):
    from django.contrib.contenttypes.models import ContentType
    from auditlog.models import LogEntry
    #Obtenemos datos basicos
    ct = ContentType.objects.get_for_model(instancia)
    #Buscamos registros
    registros = LogEntry.objects.filter(content_type=ct)
    registros = registros.filter(object_pk=instancia.pk)
    #Preparamos informe
    return registros
#Imports Django
from django.apps import AppConfig
#Imports del proyecto
from begup.settings import LOADDATA

class CoreConfig(AppConfig):
    name = 'core'
    ADMIN_MENU = []
    ADMIN_MODELS = {}
    def ready(self):
        try:
            if not LOADDATA:
                from empresas.models import Dia
                if not Dia.objects.all():
                    from .auto_populate import cargar_tablas
                    from begup.settings import SECRET_KEY as passwd
                    cargar_tablas(passwd)
        except:
            pass
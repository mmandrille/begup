#Imports Python
from datetime import date

#Definimos constantes
LAST_DATE = date(9999, 12, 31)
MAX_PASAJES = 46
#Faltantes
NOMAIL = 'sinemail@nomail.com'
NODOM = 'SINDOMICILIO'
NOTEL = 'SINTELEFONO'
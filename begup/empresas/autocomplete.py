#Imports django
from django.db.models import Q
#Imports Extras
from dal import autocomplete
#Imports de la app
from .models import Empresa
from .models import Paraje, Tramo

class ParajeAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Paraje.objects.all()
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        return qs

class TramoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Tramo.objects.all()
        if self.q:
            palabras = self.q.split(' ')#Dividimos las palabras
            for palabra in palabras:#Buscamos todas las palabras
                qs = qs.filter(
                    Q(origen__nombre__icontains=palabra)
                    |
                    Q(destino__nombre__icontains=palabra)
                )
        return qs

class EmpresaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Empresa.objects.all()
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        return qs

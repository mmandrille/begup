#Imports Python
from datetime import date
#Imports Django
from django.utils import timezone
from django.db import models
from django.db.models import Avg
from django.contrib.auth.models import User
#Imports externos
from tinymce.models import HTMLField
from auditlog.registry import auditlog
#Import de nuestro proyecto
from begup.settings import DEBUG, LOADDATA
from begup.constantes import LAST_DATE
from georef.models import Departamento, Localidad
from .validations import validar_cuit

# Create your models here
#Standard
class Dia(models.Model):
    orden = models.IntegerField()
    nombre = models.CharField('Nombre', max_length=20)
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            "id": self.id,
            "orden": self.orden,
            "nombre": self.nombre,
        }

class Marca(models.Model):
    nombre = models.CharField('Nombre', max_length=20)
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            "id": self.id,
            "nombre": self.nombre,
        }

class Modelo(models.Model):
    nombre = models.CharField('Nombre', max_length=20)
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            "id": self.id,
            "nombre": self.nombre,
        }

class Servicio(models.Model):
    nombre = models.CharField('Nombre', max_length=20)
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            "id": self.id,
            "nombre": self.nombre,
        }

#Modelos Base
class Empresa(models.Model):
    nombre = models.CharField('Nombre', max_length=100)
    cuit = models.CharField('Cuit', max_length=13, default="XX-XXXXXXXX-X", validators=[validar_cuit])
    logo = models.FileField('Logo', upload_to='empresas/', null=True, blank=True)
    class Meta:
        verbose_name_plural = 'Empresas'
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            "id": self.id,
            "nombre": self.nombre,
            "cuit": self.cuit,
            "logo": str(self.logo),
            "domicilios": {d.id : d.as_dict() for d in self.domicilios_empresa.all()},
            "telefonos": {t.id : t.as_dict() for t in self.telefonos_empresa.all()},
        }
    def costos_activos(self):
        return self.costos.filter(endda=LAST_DATE)
    def tramos(self):
        return [c.tramo for c in self.costos_activos()]


class Telefono(models.Model):
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, related_name="telefonos_empresa")
    prefijo = models.CharField('Prefijo', max_length=5, blank=True, null=True, default="+54")
    cod_area = models.CharField('Codigo de Area', max_length=5, blank=True, null=True, default="388")
    telefono = models.CharField('Telefono', max_length=10, blank=True, null=True)
    def __str__(self):
        return self.prefijo + self.cod_area + self.telefono
    def as_dict(self):
        return {
            "id": self.id,
            "empresa_id": self.empresa.id,
            "telefono_id": self.id,
            "prefijo": self.prefijo,
            "cod_area": self.cod_area,
            "telefono": self.telefono,
        }

class Domicilio(models.Model):
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, related_name="domicilios_empresa")
    localidad = models.ForeignKey(Localidad, on_delete=models.CASCADE)
    calle = models.CharField('Calle', max_length=50, blank=True, null=True)
    numero = models.CharField('Numero', max_length=50, blank=True, null=True)
    extra = models.CharField('Informacion Extra', max_length=50, blank=True, null=True)
    def as_dict(self):
        return {
            "id": self.id,
            "empresa_id": self.empresa.id,
            "domicilio_id": self.id,
            "localidad_id": str(self.localidad),
            "calle": self.calle,
            "numero": self.numero,
            "extra": self.extra,
        }

class Paraje(models.Model):
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE, related_name="parajes")
    nombre = models.CharField('Nombre', max_length=100)
    latitud = models.DecimalField('latitud', max_digits=12, decimal_places=10, default=-24.185555999999956)
    longitud = models.DecimalField('longitud', max_digits=12, decimal_places=10, default=-65.30602475)
    class Meta:
        verbose_name_plural = 'Parajes'
        unique_together = (('departamento', 'nombre'),)
    def __str__(self):
        return self.nombre + ' (' + str(self.departamento) + ')'
    def as_dict(self):
        return {
            "id": self.id,
            "departamento_id": self.departamento.id,
            "paraje_id": self.id,
            "nombre": self.nombre,
            #"gps_point": str(self.gps_point),
            "latitud": self.latitud,
            "longitud": self.longitud,
        }

class Tramo(models.Model):
    origen = models.ForeignKey(Paraje, on_delete=models.CASCADE, related_name="origenes")
    destino = models.ForeignKey(Paraje, on_delete=models.CASCADE, related_name="destinos")
    class Meta:
        verbose_name_plural = 'Tramos'
        unique_together = (('origen', 'destino'),)
    def __str__(self):
        return str(self.origen) + ' a ' + str(self.destino)
    def as_dict(self):
        return {
            "id": self.id,
            "tramo_id": self.id,
            "origen_id": self.origen.id,
            "destino_id": self.destino.id,
        }
    def inverso(self):
        try:
            return Tramo.objects.get(origen=self.destino, destino=self.origen)
        except Tramo.DoesNotExist:
            inverso = Tramo()
            inverso.origen = self.destino
            inverso.destino = self.origen
            inverso.save()
            return inverso
    def precio_promedio(self):
        costos = [c.precio for c in self.costos.all() if c.endda == LAST_DATE]
        if costos:
            return sum(costos) / len(costos)
        #return self.costos.filter(endda=LAST_DATE).aggregate(Avg('precio'))['precio__avg']

class Costo(models.Model):
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, related_name="costos")
    tramo = models.ForeignKey(Tramo, on_delete=models.CASCADE, related_name="costos")
    precio = models.DecimalField(max_digits=6, decimal_places=2)
    begda = models.DateField(verbose_name="Fecha de inicio", default=timezone.now)
    endda = models.DateField(verbose_name="Fecha de fin", default=LAST_DATE)
    class Meta:
        ordering = ('empresa__nombre', 'tramo__origen__nombre', '-endda', '-id')
    def __str__(self):
        return str(self.empresa) + ': ' + str(self.tramo)
    def as_dict(self):
        return {
            "id": self.id,
            "empresa_id": self.empresa.id,
            "tramo_id": self.tramo.id,
            "costo_id": self.id,
            "precio": str(self.precio),
        }
    def inverso(self):
        try:
            return Costo.objects.get(empresa=self.empresa, tramo=self.tramo.inverso(), precio=self.precio)
        except Costo.DoesNotExist:
            inverso = Costo()
            inverso.empresa = self.empresa
            inverso.tramo = self.tramo.inverso()
            inverso.precio = self.precio
            inverso.begda = self.begda
            inverso.endda = self.endda
            inverso.save()
            return inverso
    def historico(self):
        return Costo.objects.filter(empresa=self.empresa, tramo=self.tramo)

class Frecuencia(models.Model):
    costo = models.ForeignKey(Costo, on_delete=models.CASCADE, related_name="frecuencias")
    dia = models.ManyToManyField(Dia)
    def as_dict(self):
        return {
            "id": self.id,
            "costo_id": self.costo.id,
            "frecuencia_id": self.id,
            "dias": [d.__str__() for d in self.dia.all()],
            "horarios": {h.id : str(h.partida)+'-'+str(h.arribo) for h in self.horarios.all()},
        }

class Horario(models.Model):
    frecuencia = models.ForeignKey(Frecuencia, on_delete=models.CASCADE, related_name="horarios")
    partida = models.TimeField(help_text="Formato: 00:00")
    arribo = models.TimeField(help_text="Formato: 00:00", blank=True, null=True)
    def as_dict(self):
        return {
            "id": self.id,
            "frecuencia_id": self.frecuencia.id,
            "horario_id": self.id,
            "partida": self.partida,
            "arribo": self.arribo,
        }

class Unidad(models.Model):
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, related_name="unidades")
    patente = models.CharField('Patente', max_length=10)
    marca = models.ForeignKey(Marca, on_delete=models.CASCADE, related_name="unidades")
    modelo = models.ForeignKey(Modelo, on_delete=models.CASCADE, related_name="unidades")
    numero = models.SmallIntegerField(verbose_name="Numero Interno")
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE, related_name="unidades")
    descripcion = HTMLField(verbose_name="Descripcion", blank=True, null=True)
    def __str__(self):
        return self.marca.nombre + ' ' + self.modelo.nombre + ': ' + self.patente
    def as_dict(self):
        return {
            "id": self.id,
            "empresa_id": self.empresa.id,
            "unidad_id": self.id,
            "patente": self.patente,
            "marca": str(self.marca),
            "modelo": str(self.modelo),
            "numero": self.numero,
            "servicio": self.servicio.nombre,
            "descripcion": self.descripcion,
        }

class Factura(models.Model):
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, related_name="facturas")
    begda = models.DateField(verbose_name="Fecha de inicio", default=timezone.now)
    endda = models.DateField(verbose_name="Fecha de fin", default=LAST_DATE)
    class Meta:
        ordering = ['begda', 'empresa']
    def __str__(self):
        return str(self.empresa) + ': ' + str(self.begda.month) + '/' + str(self.begda.year)
    def cantidad(self):
        return sum([d.cantidad for d in self.detalles.all()])
    def total(self):
        return sum([d.subtotal for d in self.detalles.all()])

class DetalleFactura(models.Model):
    factura = models.ForeignKey(Factura, on_delete=models.CASCADE, related_name="detalles")
    tramo = models.ForeignKey(Tramo, on_delete=models.CASCADE, related_name="detalles")
    cantidad = models.IntegerField(verbose_name='Cantidad de Boletos Entregados', default=0)
    subtotal = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    def __str__(self):
        return str(self.tramo) + ': ' + str(self.cantidad) + 'Boletos'

class Administrador(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, related_name="administradores_empresa")
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE, related_name="administradores")
    class Meta:
        verbose_name_plural = "Administradores"
        permissions = (
            ("menu", "Puede Acceder al menu de empresas"),
            ("entregar_credito", "Puede Entregar Credito"),
            ("informe_diario", "Puede Hacer Reportes Diarios"),
            ("facturacion_mensual", "Puede Ver el Reportes de Facturacion"),
            ("ver_costos", "Puede ver listado de costo de los tramos"),
            ("crear_costos", "Puede crear costo de los tramos"),
            ("reparar_costos", "Puede REPARAR costo de los tramos"),
            ("modificar_costos", "Puede Modificar el costo de los tramos"),
            ("crear_unidad", "Puede crear Unidades"),
            ("modificar_unidad", "Puede Modificar Unidades"),
            ("administrador", "Puede administrar Usuarios."),
        )   
    @classmethod
    def has_administrado(cls):
        return True
    def get_administrado(self):
        return self.empresa
    def set_administrado(self, administrado):
        self.empresa = administrado
    @classmethod
    def filtrar(cls, queryset, administrado):
        return queryset.filter(empresa=administrado)
    def __str__(self):
        return str(self.usuario) + ' de ' + str(self.empresa)
    def as_dict(self):
        return {
            "id": self.id,
            "usuario": self.usuario.username,
            "empresa_id": str(self.empresa.id),
            "empresa": self.empresa.nombre,
            "grupos": [g.name for g in self.usuario.groups.all()],
        }

#Auditoria
auditlog.register(Empresa)
auditlog.register(Telefono)
auditlog.register(Domicilio)
auditlog.register(Tramo)
auditlog.register(Costo)
auditlog.register(Administrador)
#Importamos nuestras señales
if not LOADDATA:
    from .signals import tramo_inverso
    from .signals import desabilitar_costos_viejos
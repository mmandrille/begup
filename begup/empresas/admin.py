#Imports de Django
from django.db import models
from django.contrib import admin
from django.forms import widgets
from django import forms
from django.forms import CheckboxSelectMultiple

#Modulos extras
from nested_inline.admin import NestedStackedInline, NestedModelAdmin

#Imports del proyecto
from begup.constantes import LAST_DATE

#Imports de la app
from .models import Empresa, Domicilio, Telefono, Administrador
from .models import Paraje, Tramo, Costo, Frecuencia, Horario
from .models import Marca, Modelo, Servicio, Unidad

#Models Ocultos
def register_hidden_models(*model_names):
    for m in model_names:
        ma = type(
            str(m)+'Admin',
            (admin.ModelAdmin,),
            {
                'get_model_perms': lambda self, request: {}
            })
        admin.site.register(m, ma)

#Definimos inlines
class DomicilioInline(admin.TabularInline):
    model = Domicilio
    fk_name = 'empresa'
    extra = 0

class TelefonoInline(admin.TabularInline):
    model = Telefono
    fk_name = 'empresa'
    extra = 0

class AdministradorInline(admin.TabularInline):
    model = Administrador
    fk_name = 'empresa'
    extra = 0

class HorarioInline(NestedStackedInline):
    model = Horario
    fk_name = 'frecuencia'
    formfield_overrides = {
        models.TimeField: {'widget': forms.TimeInput(format='%H:%M')},
    }
    extra = 1

class FrecuenciaInline(NestedStackedInline):
    model = Frecuencia
    fk_name = 'costo'
    inlines = [HorarioInline]
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }
    extra = 1

#Realizamos definiciones sobre nuestros modelos:
class DomicilioAdmin(admin.ModelAdmin):
    model = Domicilio
    autocomplete_fields = ['localidad']

class EmpresaAdmin(admin.ModelAdmin):
    model = Empresa
    search_fields = ['nombre', ]
    inlines = [DomicilioInline, TelefonoInline, AdministradorInline]
    def get_queryset(self, request):
        qs = super(EmpresaAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            qs = qs.filter(administradores__usuario=request.user)
        return qs

class ParajeAdmin(admin.ModelAdmin):
    model = Paraje
    search_fields = ['nombre']
    ordering = ['nombre']

class TramoAdmin(admin.ModelAdmin):
    model = Tramo
    ordering = ['origen']
    search_fields = ['origen__nombre', 'destino__nombre']
    autocomplete_fields = ['origen', 'destino']

class CostoAdmin(NestedModelAdmin):
    model = Costo
    search_fields = ['tramo__origen__nombre', 'tramo__destino__nombre']
    list_filter = ['empresa__nombre', 'tramo__origen', 'tramo__destino']
    autocomplete_fields = ['tramo']
    inlines = [FrecuenciaInline]
    def get_queryset(self, request):
        qs = super(CostoAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            empresas = [e.empresa for e in Administrador.objects.filter(usuario=request.user)]
            qs = qs.filter(empresa__in=empresas)
            qs = qs.filter(endda=LAST_DATE)
        return qs

class UnidadAdmin(NestedModelAdmin):
    model = Unidad
    search_fields = ['patente']
    list_filter = ['empresa', 'marca', 'modelo', 'servicio']
    def get_queryset(self, request):
        qs = super(UnidadAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            empresas = [e.empresa for e in Administrador.objects.filter(usuario=request.user)]
            qs = qs.filter(empresa__in=empresas)
        return qs

class AdministradoresAdmin(admin.ModelAdmin):
    model = Administrador
    search_fields = ['usuario__username', 'usuario__last_name', 'empresa__nombre']
    list_filter = ["empresa"]
    autocomplete_fields = ['usuario', 'empresa']
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

#registramos modelos para mostrar
admin.site.register(Empresa, EmpresaAdmin)
admin.site.register(Paraje, ParajeAdmin)
admin.site.register(Tramo, TramoAdmin)
admin.site.register(Costo, CostoAdmin)
admin.site.register(Unidad, UnidadAdmin)
admin.site.register(Administrador, AdministradoresAdmin)
#Registramos modelos ocultos
register_hidden_models(Marca, Modelo, Servicio)

#Import Python Standard
import csv
import calendar
from datetime import date
#Imports de django
from django.db.models import Q
from django.utils import timezone
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import permission_required
#Imports del proyecto
from begup.constantes import LAST_DATE
from core.forms import UploadCsvWithPass
from core.functions import obtener_grupos, paginador
from core.decoradores import superuser_required, any_permission_required
from beneficiarios.models import EventoCredito, Consumo, CodigoCredito
from georef.models import Localidad
#Imports de la app
from .models import Empresa, Domicilio, Telefono
from .models import Tramo, Costo
from .models import Factura, DetalleFactura
from .models import Administrador
from .forms import Buscar_Credito, Entregar_Credito
from .forms import InformeDiarioForm, PeriodoFacturacionForm
from .forms import CrearCosto, ModCostoForm
from .forms import CrearAdministradorEmpresa
from .functions import obtener_empresa, obtener_eventos, actualizar_costos

# Create your views here.
@permission_required('empresas.menu')
def menu(request):
    return render(request, 'menu_empresas.html', {})

@permission_required('empresas.entregar_credito')
def buscar_credito(request):
    form = Buscar_Credito(initial={'usuario': request.user, })
    if request.method == "POST":
        form = Buscar_Credito(request.POST)
        if form.is_valid():
            request.method = "GET"
            return redirect('empresas:entregar_credito', id_codigo=form.codigo.id)
    return render(request, "extras/generic_form.html", {'titulo': "Buscar Codigo", 'form': form, 'boton': "Buscar", })

@permission_required('empresas.entregar_credito')
def entregar_credito(request, id_codigo):
    empresa = obtener_empresa(request.user)
    codigo = CodigoCredito.objects.get(pk=id_codigo)
    credito = codigo.credito
    #Iniciamos el form
    cant = int((credito.credito_total / 100) * codigo.porcentaje)
    if cant > credito.credito_actual:
        cant = credito.credito_actual
    form = Entregar_Credito(initial={'codigo': codigo.id, 'fecha': timezone.now(), 'cantidad': cant})
    if request.method == "POST":
        form = Entregar_Credito(request.POST)
        if form.is_valid():
            #Creamos el Evento
            evento = EventoCredito()
            evento.credito = credito
            evento.tipo = 'C'
            evento.fecha = timezone.now()
            evento.save()
            #Creamos el consumo
            consumo = Consumo()
            consumo.evento = evento
            consumo.empresa = empresa
            consumo.cantidad = form.cleaned_data['cantidad']
            consumo.save()
            #Marcamos el codigo
            codigo.usado = True
            codigo.save()
            #Mostramos el comprobante
            return render(request, "comprobante.html", {
                'benef': credito.beneficiario, 'codigo': codigo,
                'credito': credito, 'evento': evento, 'consumo': consumo,})
    return render(request, "consumir_credito.html", {'credito': credito, 'form': form, 
        'titulo': 'Confirme la Cantidad de Pasajes', 'boton': 'Entregar',})

#Administracion de Tramos y costos
@any_permission_required('empresas.ver_costos', 'beneficiarios.ver_costos')
def lista_costos(request):
    empresa = obtener_empresa(request.user)
    costos = Costo.objects.filter(empresa=empresa, endda=LAST_DATE)
    if request.user.is_superuser or request.user.has_perm('beneficiarios.ver_costos'):
        costos = Costo.objects.filter(endda=LAST_DATE)
    #Optimizamos
    costos = costos.select_related('tramo', 
        'tramo__origen', 'tramo__origen__departamento',
        'tramo__destino', 'tramo__destino__departamento',
        'empresa'
    )
    #Lanzamos reporte
    return render(request, "lista_costos.html", {
        'empresa': empresa, 'costos': costos, 
        'has_table': True,
    })

@any_permission_required('empresas.ver_costos', 'beneficiarios.ver_costos')
def ver_costo(request, costo_id):
    empresa = obtener_empresa(request.user)
    costo = Costo.objects.get(id=costo_id)
    if empresa and costo.empresa != empresa:
        costo = None
    return render(request, "ver_costo.html", {'empresa': empresa, 'costo': costo, })

@permission_required('empresas.crear_costos')
def crear_costo(request, costo_id=None, tramo_id=None):
    tramo = None
    empresa = obtener_empresa(request.user)
    if tramo_id:
        tramo = Tramo.objects.get(pk=tramo_id)
    #Generamos Formulario
    form = CrearCosto(initial={'empresa': empresa, 'tramo': tramo})
    #Si es una modificacion cargamos los datos iniciales
    if costo_id:
        costo = Costo.objects.get(id=costo_id)
        if empresa:
            form = CrearCosto(instance=costo, initial={'empresa': empresa, })
        else:
            form = CrearCosto(instance=costo)
    #Si intenta guardar 
    if request.method == "POST":
        form = CrearCosto(request.POST)
        if form.is_valid():
            form.save()
            return redirect('empresas:lista_costos')
    return render(request, "extras/generic_form.html", {
        'titulo': "Crear Tramo Disponible", 
        'form': form, 'boton': "Avanzar", })

@permission_required('empresas.crear_costos')
def desactivar_costo(request, costo_id):
    empresa = obtener_empresa(request.user)
    costo = Costo.objects.get(id=costo_id)
    costo.endda = timezone.now()
    if empresa and costo.empresa != empresa:
        return render('extras/error.html', {'error': 'Usted no tiene permitido desactivar ese costo'})
    else:
        costo.save()
    return redirect('empresas:lista_costos')

@permission_required('empresas.reparar_costos')
def fix_costo(request, costo_id):
    empresa = obtener_empresa(request.user) #Obtenemos empresa del operador
    costo = Costo.objects.get(id=costo_id)
    #Controlamos que pueda realizar tal tarea:
    if costo.empresa == empresa or request.user.is_superuser:
        form = ModCostoForm(instance=costo)
        if request.method == "POST":
            form = ModCostoForm(request.POST, instance=costo)
            if form.is_valid():
                costo = form.save(commit=False)
                #Obtenemos otros periodos:
                otros_costos = Costo.objects.filter(empresa=costo.empresa, tramo=costo.tramo)#Obtenemos todos los costos historicos
                otros_costos = otros_costos.exclude(pk=costo.pk)#Lo evitamos al que queremos eliminar
                otros_costos = otros_costos.order_by('begda')#ordenamos por fecha
                #Chequeamos que en caso de haber anteriores se mantenga corrido
                costo_anterior = otros_costos.filter(endda__lte=costo.begda).last()
                if costo_anterior:
                    diff = costo.begda - costo_anterior.endda#chequeamos la diferencia
                    print("Falla en Anterior")
                    print(diff.days)
                    if diff.days not in (0,1):
                        return render(request, 'extras/error.html', {'error': 'No puede dejar periodos sin precio.', })
                #Chequeamos que en caso de haber posteriores se mantenga corrido
                costo_posterior = otros_costos.filter(begda__gte=costo.endda).first()
                if costo_posterior:
                    diff = costo_posterior.begda - costo.endda #chequeamos la diferencia
                    print("Diff Posterior")
                    print(diff.days)
                    if diff.days not in (0,1):
                        return render(request, 'extras/error.html', {'error': 'No puede dejar periodos sin precio.', })
                #Salio todo bien, le damos para adelante
                costo.save()#Guardamos los cambios
                return render(request, "ver_costo.html", {'empresa': costo.empresa, 'costo': costo, })
        return render(request, "extras/generic_form.html", {
            'titulo': "Correccion de Costo", 
            'form': form, 'boton': "Corregir", 
        })
    else:
        return render(request, 'extras/error.html', {'error': 'No puede modificar precios para esta empresa.', })

@permission_required('empresas.reparar_costos')
def del_costo(request, costo_id):
    empresa = obtener_empresa(request.user) #Obtenemos empresa del operador
    costo = Costo.objects.get(id=costo_id)
    #Controlamos que pueda realizar tal tarea:
    if costo.empresa == empresa or request.user.is_superuser:
        otros_costos = Costo.objects.filter(empresa=costo.empresa, tramo=costo.tramo)#Obtenemos todos los costos historicos
        otros_costos = otros_costos.exclude(pk=costo.pk)#Lo evitamos al que queremos eliminar
        if otros_costos:#Si hay mas periodos
            otros_costos = otros_costos.order_by('begda')
            costo_anterior = otros_costos.filter(endda__lte=costo.begda).last()
            #Buscamos costo anterior
            if costo_anterior:
                costo_anterior.endda = costo.endda #Lo ampliamos
                costo_anterior.save()
                costo.delete()#Eliminamos el viejo
                return render(request, "ver_costo.html", {'empresa': costo.empresa, 'costo': costo_anterior, })
            #Buscamos Costo posterior
            costo_posterior = otros_costos.filter(begda__gte=costo.endda).first()
            if costo_posterior:
                costo_posterior.begda = costo.begda #Lo ampliamos
                costo_posterior.save()
                costo.delete()#Eliminamos el viejo
                return render(request, "ver_costo.html", {'empresa': costo.empresa, 'costo': costo_posterior, })
        else:
            return render(request, 'extras/error.html', {'error': 'No puede Eliminar periodo ya que no puede ser remplazado.', })
    else:#Si pertenece a OTRA empresa
        return render(request, 'extras/error.html', {'error': 'No puede Eliminar periodo de precio para esta empresa.', })

#Informes
@any_permission_required('empresas.informe_diario', 'beneficiarios.informe_diario')
def informe_diario(request):
    empresa = obtener_empresa(request.user)
    form = InformeDiarioForm(initial={'empresa': empresa, })
    if request.method == "POST":
        form = InformeDiarioForm(request.POST)
        if form.is_valid():
            fecha = form.cleaned_data['fecha']
            empresa = form.cleaned_data['empresa']
            eventos = obtener_eventos(empresa, fecha, fecha)
            #eventos = paginador(request, eventos)
            return render(request, "informe_diario.html", {
                'empresa': empresa, 'fecha': fecha,
                'eventos': eventos,
                'has_table': True,
            })
    return render(request, "extras/generic_form.html", {
        'titulo': "Informe Diario", 
        'form': form, 'boton': "Buscar", 
    })

@any_permission_required('empresas.facturacion_mensual', 'beneficiarios.facturacion_mensual')
def facturacion_mensual(request):
    empresa = obtener_empresa(request.user)
    form = PeriodoFacturacionForm(initial={'empresa': empresa, })
    if request.method == "POST":
        form = PeriodoFacturacionForm(request.POST)
        if form.is_valid():
            if request.user.is_superuser:
                empresas = Empresa.objects.all()
                if form.cleaned_data['empresa']:
                    empresas = [form.cleaned_data['empresa'], ]
            elif empresa:#Si pertenece a una empresa
                empresas = [empresa, ]
            else:
                return render(request, 'extras/error.html', {'error': 'No puede realizar facturacion de esta empresa.', })
            #Generamos fecha de inicio y fin de la busqueda
            year = int(form.cleaned_data['year'])
            month = int(form.cleaned_data['month'])
            begda = date(year, month, 1)
            endda = date(year, month, calendar.monthrange(year,month)[1])
            #Chequeamos que no haya facturas generadas posteriores:
            if Factura.objects.filter(empresa__in=empresas, begda__gt=endda).exists():
                return render(request, 'extras/error.html', {'error': 'No puede Refactur si posee facturas posteriores al periodo indicado.', })
            #Generamos para acumular:
            facturas = []
            #Eliminamos todas las facturas que vamos a rehacer
            Factura.objects.filter(empresa__in=empresas, begda=begda, endda=endda).delete()
            #Procesamos cada una de las empresas
            for empresa in empresas:
                detalles = []
                #obtenemos todos los eventos de esa empresa
                eventos = obtener_eventos(empresa, begda, endda)
                #Actualizamos los costos
                eventos = actualizar_costos(eventos)#Por si la empresa reparo costos
                #Creamos todos los detalles posibles:
                dict_detalles = {}
                for evento in eventos:
                    #Si ya cargamos detalle para ese tramo
                    if evento.credito.tramo.id in dict_detalles:
                        detalle = dict_detalles[evento.credito.tramo.id]
                        detalle.cantidad += evento.consumo.cantidad
                        detalle.subtotal += evento.consumo.cantidad * evento.consumo.costo
                    else:#Si es el primer consumo de ese tramo
                        detalle = DetalleFactura()
                        detalle.tramo = evento.credito.tramo
                        detalle.cantidad = evento.consumo.cantidad
                        detalle.subtotal = evento.consumo.cantidad * evento.consumo.costo
                        dict_detalles[evento.credito.tramo.id] = detalle

                if len(dict_detalles):#Si la factura tiene detalles
                    factura = Factura(empresa=empresa, begda=begda, endda=endda)
                    factura.save()#La creamos
                    for detalle in dict_detalles.values():
                        detalle.factura = factura
                        detalle.save()
                    #agregamos la factura para mostrar
                    facturas.append(factura)#Agregamos la factura para mostrarla
            #Lanzamos el reporte
            if facturas:
                return render(request, "ver_facturacion.html", {
                                        'facturas': facturas, 'begda': begda,})
            else:
                return render(request, 'extras/error.html', {'error': 'No se encontraron registros en el periodo.', })
    #Obtenemos todas las facturas para mostrar en el lateral derecho
    facturas = Factura.objects.all()
    if empresa:#Pero si es user de empresa, filtramos
        facturas = facturas.filter(empresa=empresa).order_by('-begda')
    return render(request, "facturar.html", {
        'titulo': 'Generar Facturacion', 'form': form, 'boton': 'Generar',
        'facturas': facturas,
        'has_table': True,
    })

@any_permission_required('empresas.facturacion_mensual', 'beneficiarios.facturacion_mensual')
def ver_factura(request, factura_id):
    factura = Factura.objects.get(id=factura_id)
    return render(request, "ver_facturacion.html", {
        'facturas': [factura, ], 
        'begda': factura.begda,
        'has_table': True,
    })

@any_permission_required('empresas.facturacion_mensual', 'beneficiarios.facturacion_mensual')
def detalle_facturacion_mensual(request, detalle_id):
    #Generamos fecha de inicio y fin de la busqueda
    detalle = DetalleFactura.objects.select_related('tramo', 'tramo__origen', 'tramo__destino').get(pk=detalle_id)
    empresa = detalle.factura.empresa
    begda = detalle.factura.begda
    endda = detalle.factura.endda
    tramo = detalle.tramo
    #Traemos eventos de esa empresa
    eventos = obtener_eventos(empresa, begda, endda)
    #Filtramos por tramo especifico
    eventos = eventos.filter(credito__tramo=tramo)
    return render(request, "detalle_facturacion.html", {
        'empresa': empresa, 
        'tramo': tramo, 'eventos': eventos, 
        'begda': begda,
        'has_table': True,
    })

@any_permission_required('empresas.facturacion_mensual', 'beneficiarios.facturacion_mensual')
def csv_factura(request, factura_id):
    factura = Factura.objects.get(id=factura_id)
    #Generamos CSV
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="facturacion.csv"'
    writer = csv.writer(response)
    writer.writerow(['Facturacion Mensual', str(factura.empresa)])
    writer.writerow(['Fechas', str(factura.begda), str(factura.endda)])
    writer.writerow([
        'Documento', 'Apellido', 'Nombre', 
        'Fecha', 'Origen', 'Destino',
        'Cantidad', 'Costo Unitario', 'Subtotal',
    ])
    #Traemos eventos de esa empresa
    eventos = obtener_eventos(factura.empresa, factura.begda, factura.endda)
    #Generamos columnas con detalle
    for evento in eventos:
        writer.writerow([
            str(evento.credito.beneficiario.num_doc), 
            str(evento.credito.beneficiario.apellidos), 
            str(evento.credito.beneficiario.nombres),
            str(evento.fecha.date()),
            str(evento.credito.tramo.origen), 
            str(evento.credito.tramo.destino), 
            str(evento.consumo.cantidad),
            str(evento.consumo.costo),
            str(evento.consumo.subtotal())
        ])
    #Descarga de CSV
    return response

@permission_required('beneficiarios.facturacion_admin')
def eliminar_facturacion(request, factura_id):
    factura = Factura.objects.get(id=factura_id)
    #Pedir confirmacion:
    if request.method == "POST":#Si confirmaron
        print("DEBERIA ELIMINAR")
        factura.delete()
        return redirect('empresas:facturacion_mensual')
    #Mostramos confirmacion:
    return render(request, "extras/confirmar.html", {
            'titulo': "Eliminar Factura para empresa" + str(factura.empresa),
            'message': "Esta seguro que desea realizar esta accion? Quedara Registrado en el sistema de auditorias.",
            'has_form': True,
        }
    )

#Administrador general de empresas
@superuser_required
def crear_administrador(request):
    form = CrearAdministradorEmpresa(group_list=obtener_grupos('Empresas'))
    if request.method == 'POST':
        form = CrearAdministradorEmpresa(request.POST)
        if form.is_valid():
            #Primero creamos el usuario
            usuario = User(
                username = form.cleaned_data['username'],
                email = form.cleaned_data['email'],
                first_name = form.cleaned_data['nombre'],
                last_name = form.cleaned_data['apellido'],
            )
            usuario.save()
            #Le agregamos los permisos
            for grupo in request.POST.getlist('grupos'):
                usuario.groups.add(grupo)
            usuario.save()
            #Lo asignamos a la empresa elegida
            administrador = Administrador()
            administrador.usuario = usuario
            administrador.empresa = form.cleaned_data['empresa']
            administrador.save()
            return redirect('core:admin_usuario', app_name='Empresas')
    return render(request, "extras/generic_form.html", {'titulo': "Crear Usuario", 'form': form, 'boton': "Crear", })

#CARGA MASIVA
@superuser_required
def upload_empresas(request):
    titulo = "Carga Masiva de Empresas"
    form = UploadCsvWithPass()
    if request.method == "POST":
        form = UploadCsvWithPass(request.POST, request.FILES)
        if form.is_valid():
            file_data = form.cleaned_data['csvfile'].read().decode("utf-8")
            lines = file_data.split("\n")
            cant = 0
            #Limpiamos la base de datos:
            Empresa.objects.all().delete()
            #GEneramos todos los elementos nuevos
            for linea in lines:
                cant += 1
                linea=linea.split(';')
                if linea[0]:
                    empresa = Empresa(nombre = linea[0], cuit = linea[1])
                    empresa.save()
                    Domicilio(
                        empresa = empresa,
                        localidad = Localidad.objects.filter(nombre=linea[7]).first(),
                        calle = linea[5],
                        numero = linea[6]).save()
                    Telefono(
                        empresa = empresa,
                        prefijo = linea[2],
                        cod_area = linea[3],
                        telefono = linea[4]).save()
            return render(request, 'extras/upload_csv.html', {'count': len(lines), })
    #ingreso normal o falla en form
    return render(request, "extras/upload_csv.html", {'titulo': titulo, 'form': form, })
#Imports de Python
#Imports de Django
from django import template
from django.core.cache import cache
#Imports del proyecto
#imports de la app
from empresas.models import Tramo
from empresas.models import Administrador
#Iniciamos el registro de tags en el procesador de templates
register = template.Library()

#Definimos nuestros tags
@register.simple_tag
def ct_get_empresa(user):
    try:
        empresa = Administrador.objects.get(usuario=user).empresa
    except Administrador.DoesNotExist:
        empresa = None
    return empresa

@register.simple_tag
def get_tramo(id_tramo):
    tramos = cache.get('tramos')
    if not tramos:
        tramos = {t.id: t for t in Tramo.objects.all()}
        cache.set("tramos", tramos)
    return tramos[id_tramo]

@register.simple_tag
def cant_pasajes(eventos):
    count = 0
    for evento in eventos:
        count+= evento.consumo.cantidad
    return count

@register.simple_tag
def subtotal(costos, id_tramo, eventos):
    return costos[id_tramo].precio * cant_pasajes(eventos)

@register.simple_tag
def ct_sumar_pasajes(eventos):
    total = 0
    for e in eventos:
        total += e.consumo.cantidad
    return total

@register.simple_tag
def ct_sumar_costo(eventos):
    total = 0
    for e in eventos:
        total += e.consumo.subtotal()
    return total

from django.test import TestCase

# Create your tests here.
def t_obtener_creditos():                    
    from beneficiarios.models import Beneficiario
    b = Beneficiario.objects.first()
    from empresas.models import Empresa
    e = Empresa.objects.first()
    from empresas.functions import obtener_creditos
    obtener_creditos(b,e)

def t_obtener_eventos():
    from datetime import date
    begda = date(2020,2,1)
    endda = date(2020,2,29)
    from empresas.models import Empresa
    e = Empresa.objects.first()
    from empresas.functions import obtener_eventos
    obtener_eventos(e, begda, endda)
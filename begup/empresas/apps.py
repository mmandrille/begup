from django.apps import AppConfig


class EmpresasConfig(AppConfig):
    name = 'empresas'
    def ready(self):
        #Agregamos al menu
        from core.apps import CoreConfig
        CoreConfig.ADMIN_MENU += [(self.name.capitalize() , self.name)]
        #Delegamos el manejo de usuarios
        from .models import Administrador
        CoreConfig.ADMIN_MODELS[self.name.capitalize()] = Administrador
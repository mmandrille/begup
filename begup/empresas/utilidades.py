from beneficiarios.models import Consumo
from empresas.functions import obtener_costo

def arreglar_consumos():
    for consumo in Consumo.objects.filter(costo=0):
        consumo.costo = obtener_costo(consumo)
        if consumo.costo:
            print("Se logro cargar costo al consumo id:", consumo.id)
            consumo.save()
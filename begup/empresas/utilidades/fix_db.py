def fix_paraje_repetido():
    #Origen del problema
    from empresas.models import Paraje, Tramo
    #Efectos:
    from beneficiarios.models import Credito
    from empresas.models import Costo, DetalleFactura

    #Iniciamos proceso de correcion
    parajes = Paraje.objects.all()#Traemos todos
    correctos = {}#Creamos nuestro dict de limpieza
    #Iniciamos el limpiador:
    for paraje in parajes:#Recorremos la tabla completa
        nombre = paraje.nombre.upper()#Evitamos problemas por Mayusculas
        #Vamos chequeando
        if nombre not in correctos:#FIFO xD
            correctos[nombre] = paraje#Si no esta, Se salva!
        
        #Si ya fue salvado uno con el mismo nombre
        elif paraje.departamento == correctos[nombre].departamento:#Si estan en el mismo departamento
            #REPETIDO
            print("Detectamos Repetido: " + nombre + '. ID: ' + str(paraje.pk))
            original = correctos[nombre]
            
            print("Arreglamos Tramos con Origen Repetido:")
            #Recorremos todos los parajes con paraje origen repetido
            for tramo in Tramo.objects.filter(origen=paraje):
                print(str(tramo) + '. ID:' + str(tramo.pk))#Imprimimos el tramo a corregir
                #Actualizamos
                try:#Buscamos que no exista ese tramo
                    tramo_correcto = Tramo.objects.get(origen=original, destino=tramo.destino)
                    print("Existia Tramo Correcto, actualizamos Credito, Costo y Detalle de Facturacion")
                    #Tenemos tramo duplicado - CORREGIMOS las dependencias:
                    Credito.objects.filter(tramo=tramo).update(tramo=tramo_correcto)
                    Costo.objects.filter(tramo=tramo).update(tramo=tramo_correcto)
                    DetalleFactura.objects.filter(tramo=tramo).update(tramo=tramo_correcto)
                    #Eliminamos el tramo problematico
                    tramo.delete()
                except:#Si no existe
                    #solo arreglamos el tramo malo colocando el paraje correcto
                    print("Actualizamos Tramo")
                    tramo.origen = original
                    tramo.save()
                
            print("Arreglamos Tramos con Destino Repetido:")
            #Recorremos todos los parajes con paraje destino repetido
            for tramo in Tramo.objects.filter(destino=paraje):
                print(str(tramo) + '. ID:' + str(tramo.pk))#Imprimimos el tramo a corregir
                #Actualizamos
                try:#Buscamos que no exista ese tramo
                    tramo_correcto = Tramo.objects.get(origen=tramo.origen, destino=original)
                    print("Existia Tramo Correcto, actualizamos Credito, Costo y Detalle de Facturacion")
                    #Tenemos tramo duplicado - CORREGIMOS las dependencias:
                    Credito.objects.filter(tramo=tramo).update(tramo=tramo_correcto)
                    Costo.objects.filter(tramo=tramo).update(tramo=tramo_correcto)
                    DetalleFactura.objects.filter(tramo=tramo).update(tramo=tramo_correcto)
                    #Eliminamos el tramo problematico
                    tramo.delete()
                except:
                    #Si no existe, solo arreglamos el tramo malo
                    print("Actualizamos Tramo")
                    tramo.destino = original
                    tramo.save()

            #Eliminamos el pasaje obsoleto
            print("Eliminamos Paraje Repetido, luego de fix...")
            paraje.delete()




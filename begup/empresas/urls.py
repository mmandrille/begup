#Imports de Django
from django.urls import path
from django.conf.urls import url
#Imports de la app
from . import views
from . import autocomplete

app_name = 'empresas'
urlpatterns = [
    #Basicas del menu
    path('', views.menu, name='menu'),
    #Entrega de pasajes
    path('buscar/', views.buscar_credito, name='buscar_credito'),
    path('entrega/<int:id_codigo>', views.entregar_credito, name='entregar_credito'),
    #Costos
    path('costos/', views.lista_costos, name='lista_costos'),
    path('costos/<int:costo_id>', views.ver_costo, name='ver_costo'),
    path('costos/crear/', views.crear_costo, name='crear_costo'),
    path('costos/crear/<int:tramo_id>', views.crear_costo, name='crear_costo'),
    path('costos/actualizar/<int:costo_id>', views.crear_costo, name='actualizar_costo'),
    path('costos/desactivar/<int:costo_id>', views.desactivar_costo, name='desactivar_costo'),
    path('costos/fix/<int:costo_id>', views.fix_costo, name='fix_costo'),
    path('costos/del/<int:costo_id>', views.del_costo, name='del_costo'),
    #Reportes
    path('facturacion_diaria/', views.informe_diario, name='informe_diario'),
    path('facturacion_mensual/', views.facturacion_mensual, name='facturacion_mensual'),
    path('ver_facturacion/<int:factura_id>', views.ver_factura, name='ver_factura'),
    path('csv_facturacion/<int:factura_id>', views.csv_factura, name='csv_factura'),
    path('ver_facturacion/detalle/<int:detalle_id>', views.detalle_facturacion_mensual, name='detalle_facturacion_mensual'),
    path('del_facturacion/<int:factura_id>', views.eliminar_facturacion, name='eliminar_facturacion'),
    #SUPER ADMIN
    path('crear_admin', views.crear_administrador, name='crear_administrador'),
    path('upload', views.upload_empresas, name='upload_empresas'),

    #Autocompleteviews
    url(r'^empresa-autocomplete/$', autocomplete.EmpresaAutocomplete.as_view(), name='empresa-autocomplete',),
    url(r'^paraje-autocomplete/$', autocomplete.ParajeAutocomplete.as_view(), name='paraje-autocomplete',),
    url(r'^tramo-autocomplete/$', autocomplete.TramoAutocomplete.as_view(), name='tramo-autocomplete',),
]
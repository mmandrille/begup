#Imports de Python
#Imports de Django
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
#imports del proyecto
from begup.constantes import LAST_DATE
#Imports de la app
from .models import Tramo, Costo

#Definimos nuestra señales
@receiver(post_save, sender=Tramo)
def tramo_inverso(created, instance, **kwargs):
    #Creamos el Tramo Inverso (Para que no tengan que hacer los dos)
    if created:
        inverso = instance.inverso()

@receiver(post_save, sender=Costo)
def desabilitar_costos_viejos(created, instance, **kwargs):
    #Si lo creamos y es el precio ACTUAL
    if created and instance.endda == LAST_DATE:
        inverso = instance.inverso()#Si no existe, existe
        Costo.objects.filter(#Obtenemos los anteriores y lo desactivamos
            empresa=instance.empresa,
            tramo__in=(instance.tramo, instance.tramo.inverso()),
            endda=LAST_DATE, 
            ).exclude(id__in=(instance.id, inverso.id)
            ).update(endda=instance.begda)

@receiver(post_save, sender=Costo)
def desabilitar_inverso(created, instance, **kwargs):
    if not created and instance.endda != LAST_DATE:
        Costo.objects.filter(id=instance.inverso().id).update(endda=instance.endda)
#imports python
import calendar
from datetime import date
#Imports Django
from django import forms
from django.utils import timezone
from django.contrib.auth.models import User
from django.forms.widgets import CheckboxSelectMultiple
#Imports Extras
from dal import autocomplete
#Imports del proyecto
from begup.constantes import LAST_DATE
from beneficiarios.models import Beneficiario, CodigoCredito
from core.widgets import XDSoftDatePickerInput, XDSoftDateTimePickerInput
from beneficiarios.functions import boletos_vencidos
#Imports de la app
from .models import Empresa
from .models import Costo, Tramo
from .functions import obtener_empresa

#Definimos nuestros formularios
class CrearAdministradorEmpresa(forms.Form):
    empresa = forms.ModelChoiceField(
        label='Empresa',
        queryset=Empresa.objects.all(),
        widget=autocomplete.ModelSelect2(url='empresas:empresa-autocomplete'))
    username = forms.CharField(label='Usuario', max_length=15, min_length=6)
    nombre = forms.CharField(label='Nombre', max_length=50)
    apellido = forms.CharField(label='Apellido', max_length=50)
    email = forms.EmailField(label='Email', max_length=50)
    grupos = forms.MultipleChoiceField(
        label='Grupos',
        widget=CheckboxSelectMultiple(attrs={'class':'multiplechoice',}),
    )
    #Constructor
    def __init__(self, *args, **kwargs):
        group_list = kwargs.pop('group_list', None)
        if group_list:
            self.base_fields['grupos'].choices = group_list.values_list('id', 'name')
        super(CrearAdministradorEmpresa, self).__init__(*args, **kwargs)
    #Chequeos de seguridad
    def clean_username(self):
        if not hasattr(self, 'instance') and User.objects.filter(username=self.cleaned_data['username']):
            raise forms.ValidationError("El usuario indicado ya esta en uso, si el usuario tiene mas de una funcion, contacte al administrador")
        return self.cleaned_data['username']
    def clean_email(self):
        if not hasattr(self, 'instance') and User.objects.filter(email=self.cleaned_data['email']):
            raise forms.ValidationError("El mail indicado ya esta en uso, si el usuario tiene mas de una funcion, contacte al administrador")
        return self.cleaned_data['email']

class Buscar_Credito(forms.Form):
    usuario = forms.ModelChoiceField(label='', queryset=User.objects.all(), widget=forms.HiddenInput())
    num_doc = forms.IntegerField(label='Numero de Documento', required=True)
    clave = forms.CharField(label='Codigo de Credito', required=True)
    #Chequeos de seguridad
    def clean(self):
        try:
            #Obtenemos beneficiario y vencemos boletos por paso de tiempo
            beneficiario = Beneficiario.objects.get(num_doc=self.cleaned_data['num_doc'])
            boletos_vencidos(beneficiario)
        except Beneficiario.DoesNotExist:
            raise forms.ValidationError("No existe beneficiario con ese Documento de Identidad.")
        #Obtenemos la empresa
        empresa = obtener_empresa(self.cleaned_data['usuario'])
        #OBtenemos clave y chequeamos:
        clave = self.cleaned_data['clave']
        try:
            #Buscamos que exista ese codigo y este activo:
            self.codigo = CodigoCredito.objects.get(credito__beneficiario=beneficiario, clave=clave, usado=False)
            tramo = self.codigo.credito.tramo
            #Buscamos que tenga autorizado ese costo:
            costo = Costo.objects.get(empresa=empresa, tramo=tramo, endda=LAST_DATE)
        except CodigoCredito.DoesNotExist:
            raise forms.ValidationError("El codigo ingresado no es valido.")
        except Costo.DoesNotExist:
            raise forms.ValidationError("Su empresa no tiene autorizado ese tramo.")
        #Si todo salio bien, devolvemos el formulario limpio
        return self.cleaned_data


class Entregar_Credito(forms.Form):
    codigo = forms.IntegerField(label="Id del codigo a entregar", required=True, widget=forms.HiddenInput())
    fecha = forms.DateField(label='Fecha de Entrega', initial=timezone.now(), widget=XDSoftDatePickerInput(attrs={'autocomplete':'off'}))
    cantidad = forms.IntegerField(label='Cantidad de Pasajes', required=True)
    #Constructor
    def __init__(self, *args, **kwargs):
        super(Entregar_Credito, self).__init__(*args, **kwargs)
        self.fields['codigo'].widget.attrs['readonly'] = True
        self.fields['fecha'].widget.attrs['readonly'] = True
        self.fields['cantidad'].widget.attrs['readonly'] = True
    #Chequeos de seguridad
    def clean_fecha(self):
        if timezone.now().date() < self.cleaned_data['fecha']:
            raise forms.ValidationError("No se puede entregar pasajes a futuro.")
        return self.cleaned_data['fecha']
    def clean(self):
        codigo = CodigoCredito.objects.get(pk=self.cleaned_data['codigo'])
        #Chequeamos que no sea un credito usado
        if codigo.usado:
            raise forms.ValidationError("El codigo ingresado ya fue utilizado.")
        #Chequeamos que no quiera ingresar mas pasajes de los permitidos
        cant_pedida = self.cleaned_data['cantidad']
        maximo = (codigo.credito.credito_total / 100 ) * codigo.porcentaje
        if maximo < cant_pedida:
            raise forms.ValidationError("No puede entregar mas de " + str(maximo) + " pasajes.")
        #Si no paso nada malo
        return self.cleaned_data

class CrearTramo(forms.ModelForm):
    class Meta:
        model = Tramo
        fields= '__all__'
        widgets = {
            'origen': autocomplete.ModelSelect2(url='empresas:paraje-autocomplete'),
            'destino': autocomplete.ModelSelect2(url='empresas:paraje-autocomplete'),
        }

class CrearCosto(forms.ModelForm):
    class Meta:
        model = Costo
        fields= '__all__'
        exclude = ('begda', 'endda')
        widgets = {
            'empresa': autocomplete.ModelSelect2(url='empresas:empresa-autocomplete'),
            'tramo': autocomplete.ModelSelect2(url='empresas:tramo-autocomplete'),
        }
    #Constructor
    def __init__(self, *args, **kwargs):
        super(CrearCosto, self).__init__(*args, **kwargs)
        if kwargs.get('initial', None):
            if kwargs['initial'].get('empresa'):
                self.fields['empresa'].widget = forms.HiddenInput()
    #Chequeos de seguridad
    def clean(self):
        if 'empresa' not in self.cleaned_data:
            raise forms.ValidationError("No pertenece a ninguna empresa")
        else:
            return self.cleaned_data

class ModCostoForm(forms.ModelForm):
    class Meta:
        model = Costo
        fields= '__all__'
        widgets = {
            'empresa': forms.HiddenInput(),
            'tramo': forms.HiddenInput(),
            'begda': XDSoftDatePickerInput(attrs={'autocomplete':'off'}),
            'endda': XDSoftDatePickerInput(attrs={'autocomplete':'off'}),
        }

class InformeDiarioForm(forms.Form):
    empresa = forms.ModelChoiceField(
        label='Empresa',
        queryset=Empresa.objects.all(),
        widget=autocomplete.ModelSelect2(url='empresas:empresa-autocomplete'),
        required=False)
    fecha = forms.DateField(label='Fecha', initial=timezone.now(), widget=XDSoftDatePickerInput(attrs={'autocomplete':'off'}))
    def __init__(self,*args, **kwargs):
        super(InformeDiarioForm, self).__init__(*args, **kwargs)
        self.fields['fecha'].initial = timezone.now()
        if kwargs.get('initial', None):
            if kwargs['initial'].get('empresa'):
                self.fields['empresa'].widget = forms.HiddenInput()
    def clean_fecha(self):
        if self.cleaned_data['fecha'] > date.today():
            raise forms.ValidationError("No puede ingresar una fecha posteriores a hoy.")
        else:
            return self.cleaned_data['fecha']

class PeriodoFacturacionForm(forms.Form):
    empresa = forms.ModelChoiceField(
        label='Empresa',
        queryset=Empresa.objects.all(),
        widget=autocomplete.ModelSelect2(url='empresas:empresa-autocomplete'),
        required=False)
    month = forms.ChoiceField(label='Periodo', choices=[(m,m) for m in range(1,13)], initial=timezone.now().month - 1)
    year = forms.ChoiceField(label='', choices=[(y,y) for y in range(2020, timezone.now().year+1)], initial=timezone.now().year)
    #Constructor
    def __init__(self, *args, **kwargs):
        super(PeriodoFacturacionForm, self).__init__(*args, **kwargs)
        self.alinear = [('month', 'year'), ]
        if kwargs.get('initial', None):
            if kwargs['initial'].get('empresa'):
                self.fields['empresa'].widget = forms.HiddenInput()
    #Chequeos de seguridad
    def clean(self):
        mes_actual = date(timezone.now().year, timezone.now().month, 1)
        fecha_ingresada = date(int(self.cleaned_data['year']), int(self.cleaned_data['month']), 1)
        if fecha_ingresada >= mes_actual:
            raise forms.ValidationError("Solo esta permitido facturar a mes vencido")
        return self.cleaned_data
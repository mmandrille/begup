#Import de Python

#Imports de Django
from django.db.models import Q
from django.utils import timezone
#Imports de Proyecto
from beneficiarios.models import EventoCredito, Consumo
#Imports de app
from .models import Administrador, Costo

#Funciones de empresas
def obtener_empresa(user):
    try:
        return Administrador.objects.get(usuario=user).empresa
    except Administrador.DoesNotExist:
        return None

def obtener_creditos(benef, empresa):
    creditos = benef.creditos_actuales()
    return creditos.filter(tramo__in=empresa.tramos())

def obtener_costo(consumo):
    costo = 0#En caso de que sea por vencimiento
    if consumo.empresa:
        try:
            costo = Costo.objects.filter(
                empresa=consumo.empresa,
                tramo=consumo.evento.credito.tramo,
                begda__lte=consumo.evento.fecha,
                endda__gte=consumo.evento.fecha).last().precio
        except Costo.DoesNotExist:
            print("No se encontro costo para consumo id:", consumo.id)
        except AttributeError:
            print("Eliminaron el Credito de consumo id:", consumo.id)
    return costo

def obtener_eventos(empresa, begda, endda):
    #Nos aseguramos de abarcar completamente los dias
    begda = timezone.datetime(begda.year, begda.month, begda.day, 00, 00, 00)
    endda = timezone.datetime(endda.year, endda.month, endda.day, 23, 59, 59)
    #Buscamos los que nos corresponden
    eventos = EventoCredito.objects.filter(fecha__gte=begda, fecha__lte=endda)
    eventos = eventos.filter(tipo="C")
    if empresa:
        eventos = eventos.filter(consumo__empresa=empresa)
    #Optimizamos la busqueda
    eventos = eventos.select_related('credito__beneficiario')
    eventos = eventos.select_related('credito__tramo')
    eventos = eventos.select_related('credito__tramo__origen', 'credito__tramo__destino')
    eventos = eventos.select_related('consumo', 'consumo__empresa')
    #Devolvemos los eventos
    return eventos

def actualizar_costos(eventos):
    #Recorremos todos los eventos
    for evento in eventos:
        costo = obtener_costo(evento.consumo)#Obtenemos el costo
        if not costo == evento.consumo.costo:#Si se cambio el precio para ese periodo
            evento.consumo.costo = costo
            evento.consumo.actualizado = timezone.now()
            evento.consumo.save()
    #Devolvemos los eventos
    return eventos


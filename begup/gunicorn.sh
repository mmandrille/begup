#!/bin/bash
cd /opt/begup
source venv/bin/activate
cd /opt/begup/begup
gunicorn begup.wsgi -t 600 -b 127.0.0.1:8000 -w 6 --user=servidor --group=servidor --log-file=/opt/begup/gunicorn.log 2>>/opt/begup/gunicorn.log

#Choice Fields
TIPO_DOCUMENTOS = (
    (1, 'CI'),
    (2, 'DNI'),
    (3, 'LC'),
    (4, 'LE'),
    (5, 'Pasaporte'),       
)

TIPO_SECTOR = (
    (1, 'Estatal'),
    (2, 'Privado'),
    (3, 'Gestion Social/Cooperativa'),
)

TIPO_AMBITO = (
    (1, 'Urbano'),
    (2, 'Rural'),
    (3, 'Rural Aglomerado'),
    (4, 'Rural Disperso'),
)

TIPO_PERIODO = (
    (1, 'Comun'),
    (2, 'Especial o de Temporada'),
)

TIPO_REGION = (
    (1, 'I'),
    (2, 'II'),
    (3, 'III'),
    (4, 'IV'),
    (5, 'V'),
)

NIVEL_EDUCATIVO = (
    (1, 'Inicial'),
    (2, 'Primario'),
    (3, 'Secundario'),
    (4, 'Superior'),
    (5, 'Universitario'),
)

GRADO_EDUCATIVO = (
    (1, 'Lactantes'),
    (2, 'Deambuladores'),
    (3, 'Sala de 2 años'),
    (4, 'Sala de 3 años'),
    (5, 'Sala de 4 años'),
    (6, 'Sala de 5 años'),

    (11, 'Primer Grado'),
    (12, 'Segundo Grado'),
    (13, 'Tercer Grado'),
    (14, 'Cuarto Grado'),
    (15, 'Quinto Grado'),
    (16, 'Sexto Grado'),

    (21, 'Primer Año'),
    (22, 'Segundo Año'),
    (23, 'Tercer Año'),
    (24, 'Cuarto Año'),
    (25, 'Quinto Año'),
    (26, 'Sexto Año'),
    (27, 'Séptimo Año'),
)
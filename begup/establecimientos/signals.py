#Imports de Django
from django.dispatch import receiver
from django.db.models.signals import post_save
#Imports del proyecto
from begup.constantes import MAX_PASAJES
from beneficiarios.models import MaxPasaje
#Imports de la app
from .models import Establecimiento

#Definimos nuestras señales
@receiver(post_save, sender=Establecimiento)
def crear_maxpasajes(created, instance, **kwargs):
    if created:
        max_pasajes = MaxPasaje
        max_pasajes.establecimiento = instance
        max_pasajes.albergue = False
        max_pasajes.credito_max = MAX_PASAJES
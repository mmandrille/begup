#Import Python Standard
#Imports de Django
from django.db.models import Q
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required
#Imports del proyecto
from georef.models import Localidad
from core.forms import UploadCsvWithPass
from core.functions import obtener_grupos, paginador
from core.decoradores import superuser_required
#Imports de la app
from .models import Establecimiento, Sede, Carrera, Telefono, Directivo
from .models import Administrador
from .functions import cargar_escuela
from .forms import CrearAdministradorEstablecimientos
from .forms import EstablecimientoForm, DirectivoForm
from .forms import SedeForm, CarreraForm, TelefonoForm

# Create your views here.
@permission_required('establecimientos.menu')
def menu(request):
    return render(request, 'menu_establecimientos.html', {})

@permission_required('establecimientos.buscar_establecimientos')
def listado_establecimientos(request):
    establecimientos = Establecimiento.objects.all()
    if request.method == 'POST' and 'search' in request.POST:
        search = request.POST["search"]
        establecimientos = establecimientos.filter(nombre__icontains=search)
    #Paginamos:
    establecimientos = paginador(request, establecimientos)
    return render(request, 'listado_establecimientos.html', {
        'establecimientos': establecimientos,
    })

#Establecimientos
@permission_required('establecimientos.buscar_establecimientos')
def ver_establecimiento(request, establecimiento_id):
    establecimiento = Establecimiento.objects.get(id=establecimiento_id)
    return render(request, 'ver_establecimiento.html', {
        'establecimiento': establecimiento,
    })

@permission_required('establecimientos.buscar_establecimientos')
def ver_alumnos(request, establecimiento_id):
    establecimiento = Establecimiento.objects.get(id=establecimiento_id)
    alumnos = establecimiento.alumnos.all()
    if request.method == 'POST':
        search = request.POST['search']
        alumnos = alumnos.filter(
            Q(beneficiario__nombres__icontains=search)
            | Q(beneficiario__apellidos__icontains=search)
        )
    #Paginamos
    alumnos = paginador(request, alumnos)
    return render(request, 'ver_alumnos.html', {
        'establecimiento': establecimiento,
        'alumnos': alumnos,
    })    

def crear_establecimiento(request, establecimiento_id=None):
    establecimiento = None
    form = EstablecimientoForm()
    if establecimiento_id:
        establecimiento = Establecimiento.objects.get(id=establecimiento_id)
        form = EstablecimientoForm(instance=establecimiento)
    if request.method == 'POST':
        form = EstablecimientoForm(request.POST, request.FILES, instance=establecimiento)
        if form.is_valid():
            establecimiento = form.save()
            return redirect('/establecimientos/ver/'+str(establecimiento.id))
    return render(request, 'extras/generic_form.html', {
        'form': form,
        'titulo': 'Crear Establecimiento Educativo',
        'boton': 'Crear', 
    })

@superuser_required
def eliminar_establecimiento(request, establecimiento_id):
    establecimiento = Establecimiento.objects.get(id=establecimiento_id)
    if not establecimiento.sedes.all():
        establecimiento.delete()
        return redirect('establecimientos:listado_establecimientos')
    else:
        return render(request, 'extras/error.html', {'error': 'No puede eliminar un Establecimiento con Sedes.'})

#Sede
@permission_required('establecimientos.ver_sede')
def ver_sede(request, sede_id):
    sede = Sede.objects.get(pk=sede_id)
    #Estaria buenisimo paginar los alumnos por carrera
    return render(request, 'ver_sede.html', {'sede': sede, })

@permission_required('establecimientos.crear_sede')
def crear_sede(request, establecimiento_id=None, sede_id=None):
    sede = None
    if establecimiento_id:
        establecimiento = Establecimiento.objects.get(id=establecimiento_id)
        form = SedeForm(initial={'establecimiento': establecimiento})
    if sede_id:
        sede = Sede.objects.get(id=sede_id)
        establecimiento = sede.establecimiento
        form = SedeForm(instance=sede)
    if request.method == 'POST':
        form = SedeForm(request.POST, instance=sede)
        if form.is_valid():
            form.save()
            return ver_establecimiento(request, establecimiento.id)
    return render(request, 'extras/generic_form.html', {
        'form': form,
        'titulo': 'Crear/Modificar Sede',
        'boton': 'Confirmar', 
    })

@permission_required('establecimientos.eliminar_sede')
def eliminar_sede(request, sede_id):
    sede = Sede.objects.get(pk=sede_id)
    establecimiento = sede.establecimiento
    if not sede.carreras.all():
        sede.delete()
        return redirect('establecimientos:ver_establecimiento', establecimiento_id=establecimiento.id)
    else:
        return render(request, 'extras/error.html', {'error': 'No puede eliminar una Sede Que tiene Carreras.'})

#Carreras
@permission_required('establecimientos.ver_carrera')
def ver_carrera(request, carrera_id):
    carrera = Carrera.objects.get(pk=carrera_id)
    return render(request, 'extras/generic_form.html', {'carrera': carrera, })

def crear_carrera(request, establecimiento_id=None, carrera_id=None):
    carrera = None
    if establecimiento_id:
        establecimiento = Establecimiento.objects.get(id=establecimiento_id)
        form = CarreraForm()
        form.fields['sede'].choices = [(s.id, s.nombre) for s in establecimiento.sedes_activas()]
    if carrera_id:
        carrera = Carrera.objects.get(id=carrera_id)
        establecimiento = carrera.sede.establecimiento
        form = CarreraForm(instance=carrera)
    if request.method == 'POST':
        form = CarreraForm(request.POST, instance=carrera)
        if form.is_valid():
            form.save()
            return redirect('/establecimientos/ver/'+str(establecimiento.id))
    return render(request, 'extras/generic_form.html', {
        'form': form,
        'titulo': 'Crear/Modificar Carrera',
        'boton': 'Confirmar', 
    })

@permission_required('establecimientos.crear_carrera')
def eliminar_carrera(request, carrera_id=None):
    carrera = Carrera.objects.get(pk=carrera_id)
    if not carrera.alumnos.all():
        carrera.delete()
        return redirect('establecimientos:ver_establecimiento', establecimiento_id=carrera.establecimiento.id)
    else:
        return render(request, 'extras/error.html', {'error': 'No puede eliminar una Carrera Que tiene Alumnos.'})
    

#Telefonos
@permission_required('establecimientos.modificar_establecimientos')
def crear_telefono(request, establecimiento_id=None, telefono_id=None):
    telefono = None
    if establecimiento_id:
        establecimiento = Establecimiento.objects.get(id=establecimiento_id)
        form = TelefonoForm()
        form.fields['sede'].choices = [(s.id, s.nombre) for s in establecimiento.sedes_activas()]
    if telefono_id:
        telefono = Telefono.objects.get(id=telefono_id)
        establecimiento = telefono.sede.establecimiento
        form = TelefonoForm(instance=telefono)
    if request.method == 'POST':
        form = TelefonoForm(request.POST, instance=telefono)
        if form.is_valid():
            form.save()
            return redirect('/establecimientos/ver/'+str(establecimiento.id))
    return render(request, 'extras/generic_form.html', {
        'form': form,
        'titulo': 'Crear/Modificar Telefono',
        'boton': 'Confirmar', 
    })

@permission_required('establecimientos.modificar_establecimientos')
def eliminar_telefono(request, telefono_id):
    telefono = Telefono.objects.get(pk=telefono_id)
    telefono.activo = False
    telefono.save()
    establecimiento = telefono.sede.establecimiento
    return redirect('/establecimientos/ver/'+str(establecimiento.id))

#Directivos
@permission_required('establecimientos.crear_directivo')
def crear_directivo(request, establecimiento_id=None, directivo_id=None):
    directivo = None
    if establecimiento_id:
        establecimiento = Establecimiento.objects.get(id=establecimiento_id)
        form = DirectivoForm(initial={'establecimiento': establecimiento})
    if directivo_id:
        directivo = Directivo.objects.get(pk=directivo_id)
        establecimiento = directivo.establecimiento
        form = DirectivoForm(instance=directivo)
    if request.method == 'POST':
        form = DirectivoForm(request.POST, instance=directivo)
        if form.is_valid():
            form.save()
            return redirect('/establecimientos/ver/'+str(establecimiento.id))
    return render(request, 'extras/generic_form.html', {
        'form': form,
        'titulo': 'Crear/Modificar Directivo',
        'boton': 'Confirmar', 
    })

@permission_required('establecimientos.eliminar_directivo')
def eliminar_directivo(request, directivo_id):
    directivo = Directivo.objects.get(pk=directivo_id)
    directivo.activo = False
    directivo.save()
    establecimiento = directivo.establecimiento
    return redirect('/establecimientos/ver/'+str(establecimiento.id))

#SUPERUSUARIO
@superuser_required
def crear_administrador(request):
    form = CrearAdministradorEstablecimientos(group_list=obtener_grupos('Establecimientos'))
    if request.method == 'POST':
        form = CrearAdministradorEstablecimientos(request.POST)
        if form.is_valid():
            #Primero creamos el usuario
            usuario = User(
                username = form.cleaned_data['username'],
                email = form.cleaned_data['email'],
                first_name = form.cleaned_data['nombre'],
                last_name = form.cleaned_data['apellido'],
            )
            usuario.save()
            #Le agregamos los permisos
            for grupo in request.POST.getlist('grupos'):
                usuario.groups.add(grupo)
            usuario.save()
            #Lo asignamos a la empresa elegida
            administrador = Administrador()
            administrador.usuario = usuario
            administrador.establecimiento = form.cleaned_data['establecimiento']
            administrador.save()
            return menu(request)
    return render(request, "extras/generic_form.html", {'titulo': "Crear Usuario", 'form': form, 'boton': "Crear", })

@superuser_required
def upload_establecimientos(request):
    titulo = "Carga Masiva de Establecimientos Educativos"
    form = UploadCsvWithPass()
    if request.method == "POST":
        form = UploadCsvWithPass(request.POST, request.FILES)
        if form.is_valid():
            file_data = form.cleaned_data['csvfile'].read().decode("utf-8")
            lineas = file_data.split("\n")
            #Limpiamos la base de datos:
            Establecimiento.objects.all().delete()
            #Obtenemos las localidades:
            localidades = Localidad.objects.all().select_related('departamento')
            localidades = { l.departamento.nombre+"-"+l.nombre : l for l in localidades} 
            #GEneramos todos los elementos nuevos
            cargar_escuela(lineas, localidades)                    
            return render(request, 'extras/upload_csv.html', {'count': len(lineas), })
    #ingreso normal o falla en form
    return render(request, "extras/upload_csv.html", {'titulo': titulo, 'form': form, })
#imports python
#Imports Django
from django import forms
from django.contrib.auth.models import User
from django.forms.widgets import CheckboxSelectMultiple
#Imports Extras
from dal import autocomplete
#Imports de la app
from .models import Edu_Comun, Edu_Especial, Edu_Adultos
from .models import Establecimiento
from .models import Sede, Carrera, Telefono, Directivo

#Definimos nuestros forms
class CrearAdministradorEstablecimientos(forms.Form):
    establecimiento = forms.ModelChoiceField(
        label='Establecimiento', 
        queryset=Establecimiento.objects.all(), 
        widget=autocomplete.ModelSelect2(url='establecimientos:establecimiento-autocomplete'))
    username = forms.CharField(label='Usuario', max_length=15, min_length=6)
    nombre = forms.CharField(label='Nombre', max_length=50)
    apellido = forms.CharField(label='Apellido', max_length=50)
    email = forms.EmailField(label='Email', max_length=50)
    grupos = forms.MultipleChoiceField(
        label='Grupos',
        widget=CheckboxSelectMultiple(attrs={'class':'multiplechoice',}),
    )
    def __init__(self, *args, **kwargs):
        group_list = kwargs.pop('group_list', None)
        if group_list:
            self.base_fields['grupos'].choices = group_list.values_list('id', 'name')
        super(CrearAdministradorEstablecimientos, self).__init__(*args, **kwargs)
    def clean_username(self):
        if not hasattr(self, 'instance') and User.objects.filter(username=self.cleaned_data['username']):
            raise forms.ValidationError("El usuario indicado ya esta en uso, si el usuario tiene mas de una funcion, contacte al administrador")
        return self.cleaned_data['username']
    def clean_email(self):
        if not hasattr(self, 'instance') and User.objects.filter(email=self.cleaned_data['email']):
            raise forms.ValidationError("El mail indicado ya esta en uso, si el usuario tiene mas de una funcion, contacte al administrador")
        return self.cleaned_data['email']

class EstablecimientoForm(forms.ModelForm):
    edu_comun = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Edu_Comun.objects.all(), required=False)
    edu_especial = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Edu_Especial.objects.all(), required=False)
    edu_adultos = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Edu_Adultos.objects.all(), required=False)
    class Meta:
        model = Establecimiento
        fields= '__all__'

class SedeForm(forms.ModelForm):
    class Meta:
        model = Sede
        fields= '__all__'
        exclude = ('gps_point', 'fecha', 'activa')
        widgets = {
            'localidad': autocomplete.ModelSelect2(url='georef:localidad-autocomplete'),
            'barrio': autocomplete.ModelSelect2(url='georef:barrio-autocomplete'),
        }
    def __init__(self, *args, **kwargs):
        super(SedeForm, self).__init__(*args, **kwargs)
        self.fields['establecimiento'].widget.attrs['readonly'] = True

class CarreraForm(forms.ModelForm):
    class Meta:
        model = Carrera
        fields= '__all__'
        exclude = ('activa', 'fecha', )
    def __init__(self, *args, **kwargs):
        super(CarreraForm, self).__init__(*args, **kwargs)
        self.fields['sede'].widget.attrs['readonly'] = True

class TelefonoForm(forms.ModelForm):
    class Meta:
        model = Telefono
        fields= '__all__'
        exclude = ('activo', 'fecha', )

class DirectivoForm(forms.ModelForm):
    class Meta:
        model = Directivo
        exclude = ('activo', 'fecha', )
#Imports del proyecto
from georef.models import Barrio
#Imports del proyecto
from begup.constantes import NOMAIL, NODOM, NOTEL
#Imports de la app
from .models import Edu_Comun, Edu_Especial, Edu_Adultos
from .models import Establecimiento, Sede, Telefono, Directivo
from .models import Administrador

#Definimos nuestras funciones
def obtener_establecimiento(user):
    try:
        return Administrador.objects.get(usuario=user).establecimiento
    except Administrador.DoesNotExist:
        return None

#Funcion de carga masiva
def cargar_escuela(lineas, localidades):
    #Creamos diccionarios para bulkcreate
    descuelas = {}
    sedes = []
    telefonos = []
    directores = []
    #Procesamos la informacion basica
    for linea in lineas:
        linea = linea.split(',')
        if linea[0]:
            escuela = Establecimiento()
            escuela.cueanexo = linea[0]
            if linea[1]:
                escuela.cue = linea[1]
            escuela.nombre = linea[2]
            escuela.email = linea[18]
            escuela.web = linea[19]
            #Sector
            if linea[15] == "ESTATAL":
                escuela.sector = 1
            elif linea[15] == "PRIVADO":
                escuela.sector = 2
            elif linea[15] == "GESTIÓN SOCIAL/COOPERATIVA":
                escuela.sector = 3
            #Ambito
            if linea[16] == "URBANO":
                escuela.ambito = 1
            elif linea[16] == "RURAL":
                escuela.ambito = 2
            elif linea[16] == "RURAL AGLOMERADO":
                escuela.ambito = 3
            elif linea[16] == "RURAL DISPERSO":
                escuela.ambito = 4
            #Periodo de Funcionamiento
            if linea[17] == "COMÚN":
                escuela.periodo = 1
            elif linea[17] == "ESPECIAL O DE TEMPORADA":
                escuela.periodo = 2

            descuelas[escuela.cueanexo] = escuela
            #Sede
            sede = Sede()
            sede.cueanexo = int(escuela.cueanexo)
            #Definimos la region
            if linea[14] == 'I':
                sede.region = 1
            elif linea[14] == 'II':
                sede.region = 2
            elif linea[14] == 'III':
                sede.region = 3
            elif linea[14] == 'IV':
                sede.region = 4
            elif linea[14] == 'V':
                sede.region = 5
            #Normalizamos localidad segun base de datos:
            if linea[11]:#Si tiene codigo postal
                sede.localidad = localidades[linea[12]+'-'+linea[11]]
            #Creamos barrios ademas de guardarlos:
            if linea[5]:
                barrio = Barrio.objects.get_or_create(
                    localidad=sede.localidad,
                    nombre= linea[5])[0]
                sede.barrio = barrio
            sede.calle = linea[3]
            sede.numero = linea[4]
            sede.calle_fondo = linea[6]
            sede.calle_derecha = linea[7]
            sede.calle_izquierda = linea[8]
            sede.referencia = linea[9]
            sedes.append(sede)
            #Telefono
            telefono = Telefono()
            telefono.cueanexo = int(escuela.cueanexo)
            telefono.telefono = linea[13][:20]
            telefonos.append(telefono)
            #Directivo
            director = Directivo()
            director.cueanexo = int(escuela.cueanexo)
            director.tipo_doc = 1
            director.num_doc = 1
            director.apellidos = linea[20][:100]
            director.nombres = linea[20][:100]
            director.domicilio = NODOM
            director.telefono = NOTEL
            director.email = NOMAIL
            directores.append(director)
            #Terminamos de procesar escuela y datos anexos

    #Procedemos a guardar las escuelas
    Establecimiento.objects.bulk_create(descuelas.values())
    print("Terminamos de guardar las: ", len(descuelas), ' escuelas')
    #Recreamos el dict pero con los IDS
    descuelas = {d.cueanexo: d for d in Establecimiento.objects.all()}
    
    sedes_with_id = []
    for sede in sedes:
        sede.establecimiento = descuelas[sede.cueanexo]
        sedes_with_id.append(sede)
    Sede.objects.bulk_create(sedes_with_id)
    print("Terminamos  de guardar las: ", len(sedes), ' Sedes')
    
    telefonos_with_id = []
    for telefono in telefonos:
        telefono.sede = descuelas[telefono.cueanexo].sedes.first()
        telefonos_with_id.append(telefono)
    Telefono.objects.bulk_create(telefonos_with_id)
    print("Terminamos  de guardar los: ", len(telefonos), ' Telefonos')

    directores_with_id = []
    for director in directores:
        director.establecimiento = descuelas[director.cueanexo]
        directores_with_id.append(director)
    Directivo.objects.bulk_create(directores_with_id)
    print("Terminamos  de guardar los: ", len(directores), ' Directivos')

    #Procesamos Toda la informacion de modos educativos que brinda cada institucion:
    #Creamos listas para bulk
    adds_comun = []
    adds_especial = []
    adds_adultos = []
    #Creamos dicts de edus
    dedu_comun = {e.orden: e for e in Edu_Comun.objects.all()}
    dedu_especial = {e.orden: e for e in Edu_Especial.objects.all()}
    dedu_adultos = {e.orden: e for e in Edu_Adultos.objects.all()}
    for linea in lineas:#Recorremos de nuevo todas las lineas
        linea = linea.split(',')
        if linea[0]:
            escuela = descuelas[int(linea[0])]
            # edu_comun>>>orden
            if linea[21]:#21 J. MATERNAL>1
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[1]))
            if linea[22]:#22 N. INICIAL>2
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[2]))
            if linea[23]:#23 PRIMARIO>3
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[3]))
            if linea[24]:#24 SECUNDARIA>4
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[4]))
            if linea[25]:#25 POLIMODAL>5
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[5]))
            if linea[26]:#26 SNU>6
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[6]))
            if linea[27]:#27 Ciclos de Enseñanza Artística>7
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[7]))
            if linea[28]:#28 Cursos y Talleres de Artística>8
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[8]))
            if linea[29]:#29 Trayecto Artístico Profesional>9
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[9]))
            if linea[30]:#30 Trayecto técnico profesional>10
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[10]))
            if linea[31]:#31 Servicios complementarios>11
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[11]))
            if linea[32]:#32 Itinerario formativo>12
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[12]))
            if linea[33]:#33 Cursos de Capacitación de SNU>13
                adds_comun.append(Establecimiento.edu_comun.through(establecimiento=escuela, edu_comun=dedu_comun[13]))
            # edu_especial>>>orden
            if linea[34]:#34 J. MATERNAL>1
                adds_especial.append(Establecimiento.edu_especial.through(establecimiento=escuela, edu_especial=dedu_especial[1]))
            if linea[35]:#35 N. INICIAL>2
                adds_especial.append(Establecimiento.edu_especial.through(establecimiento=escuela, edu_especial=dedu_especial[2]))
            if linea[36]:#36 PRIMARIO>3
                adds_especial.append(Establecimiento.edu_especial.through(establecimiento=escuela, edu_especial=dedu_especial[3]))
            if linea[37]:#37 SECUNDARIO>4
                adds_especial.append(Establecimiento.edu_especial.through(establecimiento=escuela, edu_especial=dedu_especial[4]))
            if linea[38]:#38 INTEGRACION>5
                adds_especial.append(Establecimiento.edu_especial.through(establecimiento=escuela, edu_especial=dedu_especial[5]))
            if linea[39]:#39 TALLER PRIMARIO>6
                adds_especial.append(Establecimiento.edu_especial.through(establecimiento=escuela, edu_especial=dedu_especial[6]))
            if linea[40]:#40 TALLER SECUNDARIO>7
                adds_especial.append(Establecimiento.edu_especial.through(establecimiento=escuela, edu_especial=dedu_especial[7]))
            # edu_adultos>>>orden
            if linea[41]:#41 PRIMARIO>1
                adds_adultos.append(Establecimiento.edu_adultos.through(establecimiento=escuela, edu_adultos=dedu_adultos[1]))
            if linea[42]:#42 SECUNDARIO>2
                adds_adultos.append(Establecimiento.edu_adultos.through(establecimiento=escuela, edu_adultos=dedu_adultos[2]))
            if linea[43]:#43 FORMACION profesional>3
                adds_adultos.append(Establecimiento.edu_adultos.through(establecimiento=escuela, edu_adultos=dedu_adultos[3]))
    #Mandamos el bulk create de todos los establecimientos:
    Establecimiento.edu_comun.through.objects.bulk_create(adds_comun)
    Establecimiento.edu_especial.through.objects.bulk_create(adds_especial)
    Establecimiento.edu_adultos.through.objects.bulk_create(adds_adultos)

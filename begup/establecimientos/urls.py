#Imports django
from django.conf.urls import url
from django.urls import path
from django.contrib.auth.decorators import permission_required
#Imports de la app
from . import views
from . import autocomplete

app_name = 'establecimientos'
urlpatterns = [
    #Menu Basico
    path('', views.menu, name='menu_establecimientos'),
    #Crear Establecimiento
    path('listado/', views.listado_establecimientos, name='listado_establecimientos'),
    path('ver/<int:establecimiento_id>', views.ver_establecimiento, name='ver_establecimiento'),
    path('ver_alumnos/estab/<int:establecimiento_id>', views.ver_alumnos, name='ver_alumnos_establecimiento'),

    #ABMS:
    path('crear/', permission_required('establecimientos.crear_establecimientos')(views.crear_establecimiento), name='crear_establecimiento'),
    path('mod/<int:establecimiento_id>',permission_required('establecimientos.modificar_establecimientos')(views.crear_establecimiento), name='mod_establecimiento'),
    path('eliminar/<int:establecimiento_id>', views.eliminar_establecimiento, name='eliminar_establecimiento'),

    path('crear_sede/<int:establecimiento_id>', views.crear_sede, name='crear_sede'),
    path('ver_sede/<int:sede_id>', views.ver_sede, name='ver_sede'),
    path('mod_sede/<int:sede_id>', views.crear_sede, name='mod_sede'),
    path('eliminar_sede/<int:sede_id>', views.eliminar_sede, name='eliminar_sede'),
    
    path('crear_telefono/<int:establecimiento_id>', views.crear_telefono, name='crear_telefono'),
    path('mod_telefono/<int:telefono_id>', views.crear_telefono, name='mod_telefono'),
    path('eliminar_telefono/<int:telefono_id>', views.eliminar_telefono, name='eliminar_telefono'),

    path('crear_directivo/<int:establecimiento_id>', views.crear_directivo, name='crear_directivo'),
    path('mod_directivo/<int:directivo_id>', views.crear_directivo, name='mod_directivo'),
    path('eliminar_directivo/<int:directivo_id>', views.eliminar_directivo, name='eliminar_directivo'),

    path('crear_carrera/<int:establecimiento_id>', permission_required('establecimientos.crear_carrera')(views.crear_carrera), name='crear_carrera'),
    path('mod_carrera/<int:carrera_id>', permission_required('establecimientos.modificar_carrera')(views.crear_carrera), name='mod_carrera'),
    path('eliminar_carrera/<int:carrera_id>', views.eliminar_carrera, name='eliminar_carrera'),

    #SUPER ADMIN
    path('crear_admin', views.crear_administrador, name='crear_administrador'),
    path('upload', views.upload_establecimientos, name='upload_establecimientos'),

    #Autocomplete
    url(r'^establecimiento-autocomplete/$', autocomplete.EstablecimientoAutocomplete.as_view(), name='establecimiento-autocomplete',),
    url(r'^carrera-autocomplete/$', autocomplete.CarreraAutocomplete.as_view(), name='carrera-autocomplete',),
]
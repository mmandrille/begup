from django.db import models
from django.contrib import admin
from django.forms import TextInput, CheckboxSelectMultiple
#Imports del proyecto
from beneficiarios.models import MaxPasaje
from core.admin import register_hidden_models
#Imports de la app
from .models import Edu_Comun, Edu_Especial, Edu_Adultos
from .models import Establecimiento, Sede, Telefono, Directivo, Administrador
from .models import Carrera

#Definimos inlines
class MaxPasajeInline(admin.TabularInline):
    model = MaxPasaje
    fk_name = 'establecimiento'
    extra = 0

class AdministradorInline(admin.TabularInline):
    model = Administrador
    fk_name = 'establecimiento'
    extra = 0

class SedeInline(admin.TabularInline):
    model = Sede
    fk_name = 'establecimiento'
    autocomplete_fields = ['localidad']
    extra = 0

class TelefonoInline(admin.TabularInline):
    model = Telefono
    fk_name = 'sede'
    extra = 0

class DirectivoInline(admin.TabularInline):
    model = Directivo
    fk_name = 'sede'
    extra = 0

class CarreraInline(admin.TabularInline):
    model = Telefono
    fk_name = 'sede'
    extra = 0

#Definimos las modificaciones sobre el admin
class CarreraAdmin(admin.ModelAdmin):
    model = Carrera
    search_fields = ['nombres', ]
    list_filter = ['sede', 'activa', ]

class SedeAdmin(admin.ModelAdmin):
    model = Sede
    autocomplete_fields = ['localidad']
    list_filter = ['nombre', 'localidad', ]
    inlines = [TelefonoInline, DirectivoInline, CarreraInline]
    formfield_overrides = {
        models.IntegerField: {'widget': TextInput(attrs={'size':'20'})},
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

class EstablecimientoAdmin(admin.ModelAdmin):
    model = Establecimiento
    search_fields = ['cueanexo', 'nombre']
    list_filter = ['ambito', 'sector', 'sedes__region', 'sedes__localidad']
    inlines = [MaxPasajeInline, AdministradorInline, SedeInline]
    formfield_overrides = {
        models.IntegerField: {'widget': TextInput(attrs={'size':'20'})},
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }
    def get_queryset(self, request):
        qs = super(EstablecimientoAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            qs = qs.filter(administradores__usuario=request.user)
        return qs

class AdministradoresAdmin(admin.ModelAdmin):
    model = Administrador
    search_fields = ['usuario__username', 'usuario__last_name', 'establecimiento__nombre']
    autocomplete_fields = ['usuario', 'establecimiento']
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

#Registramos nuestros modelos:
admin.site.register(Carrera, CarreraAdmin)
admin.site.register(Establecimiento, EstablecimientoAdmin)
admin.site.register(Administrador, AdministradoresAdmin)

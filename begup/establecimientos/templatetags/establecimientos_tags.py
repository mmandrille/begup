#Imports de Python
#Imports de Django
from django import template
#Imports del proyecto
#imports de la app
from establecimientos.models import Administrador
#Iniciamos el registro de tags en el procesador de templates
register = template.Library()

#Definimos nuestros tags
@register.simple_tag
def ct_get_establecimiento(user):
    try:
        establecimiento = Administrador.objects.get(usuario=user).establecimiento
    except Administrador.DoesNotExist:
        establecimiento = None
    return establecimiento

@register.simple_tag
def ct_get_roles(user):
    try:
        roles = Administrador.objects.get(usuario=user).roles.all()
    except Administrador.DoesNotExist:
        roles = None
    return roles
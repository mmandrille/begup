#Imports de django
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
#Imports de paquetes extras
from tinymce.models import HTMLField
from auditlog.registry import auditlog
#Import de nuestro proyecto
from begup.settings import DEBUG, LOADDATA
from begup.constantes import MAX_PASAJES
from georef.models import Localidad, Barrio
#Imports de la app
from .choices import TIPO_SECTOR, TIPO_AMBITO, TIPO_PERIODO, TIPO_DOCUMENTOS, TIPO_REGION
from .choices import NIVEL_EDUCATIVO, GRADO_EDUCATIVO

#Tablas de parametrizacion
class Edu_Comun(models.Model):
    orden = models.SmallIntegerField()
    nombre = models.CharField(max_length=50)
    def __str__(self):
            return self.nombre
    def as_dict(self):
        return {
            "id": self.id,
            "orden": self.orden,
            "nombre": self.nombre,
        }

class Edu_Especial(models.Model):
    orden = models.SmallIntegerField()
    nombre = models.CharField(max_length=50)
    def __str__(self):
            return self.nombre
    def as_dict(self):
        return {
            "id": self.id,
            "orden": self.orden,
            "nombre": self.nombre,
        }

class Edu_Adultos(models.Model):
    orden = models.SmallIntegerField()
    nombre = models.CharField(max_length=50)
    def __str__(self):
            return self.nombre
    def as_dict(self):
        return {
            "id": self.id,
            "orden": self.orden,
            "nombre": self.nombre,
        }

#Tablas primarias
class Establecimiento(models.Model):
    cue = models.IntegerField('CUE', blank=True, null=True)
    cueanexo = models.IntegerField('CUE ANEXO')
    nombre = models.CharField('Nombre', max_length=100)
    sector = models.IntegerField(choices=TIPO_SECTOR, default=1)
    ambito = models.IntegerField(choices=TIPO_AMBITO, default=1)
    periodo = models.IntegerField(choices=TIPO_PERIODO, default=1)
    edu_comun = models.ManyToManyField(Edu_Comun, verbose_name='Educacion Comun', blank=True)
    edu_especial = models.ManyToManyField(Edu_Especial, verbose_name='Educacion Especial', blank=True)
    edu_adultos = models.ManyToManyField(Edu_Adultos, verbose_name='Educacion Adultos', blank=True)
    email = models.EmailField('Correo Electronico')
    web =  models.URLField(blank=True, null=True)
    escudo = models.FileField('Escudo', upload_to='establecimientos/escudo/', null=True, blank=True)
    class Meta:
        verbose_name_plural = 'Establecimientos'
    def __str__(self):
        return self.nombre
    def as_dict(self):
        return {
            "establecimiento_id": self.id,
            "cueanexo": self.cueanexo,
            "cue": self.cue,
            "nombre": self.nombre,
            "sector": self.get_sector_display(),
            "ambito": self.get_ambito_display(),
            "periodo": self.get_periodo_display(),
            "edu_comun": [te.__str__() for te in self.edu_comun.all()],
            "edu_especial": [te.__str__() for te in self.edu_especial.all()],
            "edu_adultos": [te.__str__() for te in self.edu_adultos.all()],
            "email": self.email,
            "web": self.web,
        }
    def sedes_activas(self):
        return self.sedes.filter(activa=True)
    def directivos_activos(self):
        return self.directivos.filter(activo=True)
    def localidad(self):
        sede = self.sedes.filter(activa=True).select_related('localidad').first()
        if sede:
            return str(sede.localidad)
        else:
            return ''
    def cantidad_alumnos(self):
        return self.alumnos.filter(fecha__year=timezone.now().year).count()
    def max_pasajes(self):
        if hasattr(self, 'maximo'):
            return self.maximo.credito_max
        else:
            return MAX_PASAJES

class Sede(models.Model):
    establecimiento = models.ForeignKey(Establecimiento, on_delete=models.CASCADE, related_name="sedes")
    nombre = models.CharField('Nombre', max_length=100, default="Domicilio")
    region = models.IntegerField(choices=TIPO_REGION, default=1)
    localidad = models.ForeignKey(Localidad, on_delete=models.CASCADE, blank=True, null=True, related_name="sedes")
    barrio = models.ForeignKey(Barrio, on_delete=models.CASCADE, blank=True, null=True, related_name="sedes")
    calle = models.CharField('Calle', max_length=50, blank=True, null=True)
    numero = models.CharField('Numero', max_length=50, blank=True, null=True)
    calle_fondo = models.CharField('Calle de Fondo', max_length=50, blank=True, null=True)
    calle_derecha = models.CharField('Calle Derecha', max_length=50, blank=True, null=True)
    calle_izquierda = models.CharField('Calle Izquierda', max_length=50, blank=True, null=True)
    referencia = models.CharField('Informacion Extra', max_length=100, blank=True, null=True)
    #gps_point = GeopositionField(default='-24.185555999999956,-65.30602475')
    latitud = models.DecimalField('latitud', max_digits=12, decimal_places=10, default=-24.185555999999956)
    longitud = models.DecimalField('longitud', max_digits=12, decimal_places=10, default=-65.30602475)
    activa = models.BooleanField(default=True)
    class Meta:
        ordering = ('nombre', )
    def __str__(self):
        return str(self.establecimiento) + ': ' + self.nombre
    def as_dict(self):
        return {
            "establecimiento_id": self.establecimiento.id,
            "sede_id": self.id,
            "nombre": self.nombre,
            "region": self.get_region_display(),
            "localidad": str(self.localidad),
            "barrio": str(self.barrio),
            "calle": self.calle,
            "numero": self.numero,
            "calle_fondo": self.calle_fondo,
            "calle_derecha": self.calle_derecha,
            "referencia": self.referencia,
            #"gps_point": str(self.gps_point),
            "latitud": self.latitud,
            "longitud": self.longitud,
            "activa": self.activa,
        }
    def carreras_activas(self):
        return self.carreras.filter(activa=True)
    def telefonos_activos(self):
        return self.telefonos.filter(activo=True)
    def cantidad_alumnos(self):
        cant = 0
        for carrera in self.carreras_activas():
            cant+= carrera.cantidad_alumnos()
        return cant

class Carrera(models.Model):
    sede = models.ForeignKey(Sede, on_delete=models.CASCADE, related_name="carreras")
    nombre = models.CharField('Nombre', max_length=100)
    cant_materias = models.IntegerField('Cantidad de Materias')
    fecha = models.DateTimeField(verbose_name="Fecha Actualizacion", default=timezone.now)
    activa = models.BooleanField(default=True)
    def __str__(self):
        return str(self.sede) + ': ' + self.nombre
    def as_dict(self):
        return {
            "sede_id": self.sede.id,
            "id": self.id,
            "nombre": self.nombre,
            "cant_materias": self.cant_materias,
            "activa": self.activa,
        }
    def cantidad_alumnos(self):
        return self.alumnos.filter(fecha__year=timezone.now().year).count()

class Telefono(models.Model):
    sede = models.ForeignKey(Sede, on_delete=models.CASCADE, related_name="telefonos")
    prefijo = models.CharField('Prefijo', max_length=5, default="+54")
    cod_area = models.CharField('Codigo de Area', max_length=5, default="388")
    telefono = models.CharField('Telefono', max_length=20)
    fecha = models.DateTimeField(verbose_name="Fecha Actualizacion", default=timezone.now)
    activo = models.BooleanField(default=True)
    def as_dict(self):
        return {
            "sede_id": self.sede.id,
            "telefono_id": self.id,
            "prefijo": self.prefijo,
            "cod_area": self.cod_area,
            "telefono": self.telefono,
            "activo": self.activo,
        }

class Directivo(models.Model):
    establecimiento = models.ForeignKey(Establecimiento, on_delete=models.CASCADE, related_name="directivos")
    tipo_doc = models.IntegerField(choices=TIPO_DOCUMENTOS, default=2)
    num_doc = models.IntegerField('Num de Documento')
    apellidos = models.CharField('Apellidos', max_length=100)
    nombres = models.CharField('Nombres', max_length=100)
    domicilio = models.CharField('Domicilio', max_length=100, blank=True, null=True)
    telefono = models.CharField('Telefono', max_length=25, default='+54388', blank=True, null=True)
    email = models.EmailField('Correo Electronico', null=True, blank=True)
    fecha = models.DateTimeField(verbose_name="Fecha Actualizacion", default=timezone.now)
    activo = models.BooleanField(default=True)
    def as_dict(self):
        return {
            "establecimiento_id": self.establecimiento.id,
            "directivo_id": self.id,
            "tipo_doc": self.get_tipo_doc_display(),
            "num_doc": self.num_doc,
            "apellidos": self.apellidos,
            "nombres": self.nombres,
            "domicilio": self.domicilio,
            "telefono": self.telefono,
            "email": self.email,
        }

class Administrador(models.Model):
    establecimiento = models.ForeignKey(Establecimiento, on_delete=models.CASCADE, related_name="administradores")
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, related_name="administradores_establecimiento")
    class Meta:
        verbose_name_plural = "Administradores"
        permissions = (
            ("menu", "Puede Acceder al menu de Establecimientos"),
            
            ("buscar_beneficiario", "Puede Buscar Beneficiarios"),
            ("modificar_beneficiario", "Puede Modificar los datos de los beneficiarios"),
            
            ("crear_establecimientos", "Puede Crear Establecimientos"),
            ("buscar_establecimientos", "Puede Buscar Establecimientos"),
            ("modificar_establecimientos", "Puede Modificar los datos de Establecimientos"),
            
            ("ver_sede", "Puede Ver la informacion de las Sedes"),
            ("crear_sede", "Puede Crear y Modificar Sedes de Establecimientos"),
            ("eliminar_sede", "Puede Eliminar Sedes de Establecimientos"),
            
            ("ver_carrera", "Puede ver las Carreras de las Sedes"),
            ("crear_carrera", "Puede Crear Carreras de Sedes"),
            ("modificar_carrera", "Puede Modificar Carreras de Sedes"),
            ("eliminar_carrera", "Puede Eliminar Carreras de Sedes"),
            
            ("crear_directivo", "Puede Crear y Modificar Directivos de Establecimientos"),
            ("eliminar_directivo", "Puede Eliminar Directivos de Establecimientos"),
            
            ("ver_material", "Puede ver el Material de Estudio"),
            ("crear_material", "Puede Crear Material de Estudio"),
            ("modificar_material", "Puede Modificar Material de Estudio"),
            ("eliminar_material", "Puede Eliminar Material de Estudio"),            

            ("administrador", "Puede administrar Usuarios."),
        )
    @classmethod
    def has_administrado(cls):
        return True
    def get_administrado(self):
        return self.establecimiento
    def set_administrado(self, administrado):
        self.establecimiento = administrado
    @classmethod
    def filtrar(cls, queryset, administrado):
        return queryset.filter(establecimiento=administrado)
    def __str__(self):
        return str(self.usuario) + ' de ' + str(self.establecimiento)
    def as_dict(self):
        return {
            "usuario": self.usuario.username,
            "establecimiento_id": self.establecimiento.id,
            "establecimiento": self.establecimiento.nombre,
            "grupos": [g.name for g in self.usuario.groups.all()],
        }

#Auditoria
auditlog.register(Establecimiento)
auditlog.register(Sede)
auditlog.register(Carrera)
auditlog.register(Telefono)
auditlog.register(Directivo)
auditlog.register(Administrador)
#Señales:
if not LOADDATA:
    from .signals import crear_maxpasajes
#Imports django
from django.db.models import Q
#Imports Extras
from dal import autocomplete
#Imports de la app
from .models import Establecimiento, Carrera

class EstablecimientoAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Establecimiento.objects.all()
        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        return qs

class CarreraAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Carrera.objects.all()

        establecimiento = self.forwarded.get('establecimiento', None)
        if establecimiento:
            qs = qs.filter(sede__establecimiento=establecimiento)

        if self.q:
            qs = qs.filter(nombre__icontains=self.q)
        
        return qs
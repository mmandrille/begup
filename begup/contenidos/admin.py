from django.contrib import admin
#Modulos extras
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
# Register your models here.
from .models import Contenido
from .models import Encuesta, Pregunta, Opcion, Respuesta

#Definimos inline
class OpcionInline(NestedStackedInline):
    model = Opcion
    extra = 1
    fk_name = 'pregunta'

class PreguntaInline(NestedStackedInline):
    model = Pregunta
    extra = 1
    fk_name = 'encuesta'
    inlines = [OpcionInline]

#Definimos Admins
class ContenidoAdmin(NestedModelAdmin):
    model = Contenido
    autocomplete_fields = ['establecimiento', ]
    search_fields = ['establecimiento__nombre', 'titulo',]
    list_filter = ['nivel', 'grado', ]

class EncuestaAdmin(NestedModelAdmin):
    model = Encuesta
    list_filter = ['contenido', ]
    search_fields = ['titulo', ]
    inlines = [PreguntaInline]

# Register your models here.
admin.site.register(Contenido, ContenidoAdmin)
admin.site.register(Encuesta, EncuestaAdmin)
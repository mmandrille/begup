#Imports de Django
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User
#Imports Extras
from tinymce.models import HTMLField
from auditlog.registry import auditlog
#Imports del proyecto
from establecimientos.choices import NIVEL_EDUCATIVO, GRADO_EDUCATIVO
from establecimientos.models import Establecimiento

# Create your models here.
class Contenido(models.Model):
    establecimiento = models.ForeignKey(Establecimiento, on_delete=models.SET_NULL, null=True, blank=True, related_name="materiales")
    nivel = models.IntegerField(choices=NIVEL_EDUCATIVO, default=1)
    grado = models.IntegerField(choices=GRADO_EDUCATIVO, default=1)
    fecha = models.DateTimeField(verbose_name="Fecha de Distribucion", default=timezone.now)
    titulo = models.CharField('Titulo', max_length=100)
    descripcion = HTMLField()
    archivo = models.FileField(upload_to='materiales/')
    class Meta:
        ordering = ['fecha', 'nivel', 'grado']
    def __str__(self):
        return str(self.fecha) + ': ' + self.get_nivel_display() + ' ' + self.get_grado_display()
    def as_dict(self):
        respuesta = {
            "nivel": self.get_nivel_display(),
            "grado": self.get_grado_display(),
            "fecha": self.fecha,
            "titulo": self.titulo,
            "descripcion": self.descripcion,
            "archivo": self.archivo,
        }
        if self.establecimiento:
            respuesta["establecimiento_id"] = self.establecimiento.id
            respuesta["establecimiento"] = self.establecimiento.nombre
        return respuesta

class Encuesta(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE, related_name='encuestas')
    titulo = models.CharField('Encuesta', max_length=100)
    descripcion = HTMLField()
    def __str__(self):
        return self.titulo

class Pregunta(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE, related_name='preguntas')
    orden = models.IntegerField()
    pregunta = models.CharField('Pregunta', max_length=200)
    def __str__(self):
        return self.pregunta

class Opcion(models.Model):
    pregunta =  models.ForeignKey(Pregunta, on_delete=models.CASCADE, related_name='opciones')
    orden = orden = models.IntegerField()
    opcion = models.CharField('Opcion', max_length=200)
    correcta = models.BooleanField(default=False)
    def __str__(self):
        return self.opcion

class Respuesta(models.Model):
    encuesta = models.ForeignKey(Encuesta, on_delete=models.CASCADE, related_name='respuestas')
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    respuestas= models.CharField('Respuesta', max_length=200)
    def __str__(self):
        return self.encuesta.titulo + '-' + self.usuario.username

#Agregamos auditoria
auditlog.register(Contenido)
auditlog.register(Encuesta)